﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Timers;
namespace FloodAutomation.Hubs
{
    public class SMSHub : Hub
    {
        [HubMethodName("NotifyClients")]
        public static void NotifyDataInformationToAllClients()
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<SMSHub>();
            context.Clients.All.updatedClients();
        }
    }

    public class MessageHub :Hub
    {
        [HubMethodName("NotifyClients")]
        public static void NotifyDataMessageToAllClients()
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<MessageHub>();
            context.Clients.All.updatedClients();
        }
    }

    public class ReadSMS : Hub
    {
        [HubMethodName("NotifyClients")]
        public static void NotifyDataReadToAllClients()
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<ReadSMS>();
            context.Clients.All.updatedClients();
        }
    }

}