﻿
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;
using System;
using System.Windows.Forms;
[assembly: OwinStartupAttribute(typeof(FloodAutomation.Startup))]
namespace FloodAutomation
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);
            app.MapSignalR();
            ConfigureAuth(app);
        }
    }
}
