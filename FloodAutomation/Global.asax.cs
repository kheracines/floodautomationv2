﻿using FloodAutomation.App_Start;
using FloodAutomation.Hubs;
using FloodAutomation.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace FloodAutomation
{

    public class MvcApplication : System.Web.HttpApplication
    {
     
        protected void Application_Start()
        {
            Application["Totaluser"] = 0;
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            AutoMapperConfig.RegisterMappings();
           // InitializeAdminUser();
        }

        protected void Session_OnStart()
        {
          
            Application.Lock();
            Application["Totaluser"] = (int)Application["Totaluser"] + 1;
            Application.UnLock();
            MessageHub.NotifyDataMessageToAllClients();
        }

        protected void Session_OnEnd()
        {

            Application.Lock();
            Application["Totaluser"] = (int)Application["Totaluser"] - 1;
            Application.UnLock();
            MessageHub.NotifyDataMessageToAllClients();

        }
        private void InitializeAdminUser()
        {
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
            string password = "adminadmin";
            string role = "Admin";

            if (!RoleManager.RoleExists(role))
            {
                var roleresult = RoleManager.Create(new IdentityRole(role));
            }
            if (!RoleManager.RoleExists("Admin"))
            {
                var roleresult = RoleManager.Create(new IdentityRole("Admin"));
            }
            if (!RoleManager.RoleExists("User"))
            {
                var roleresult = RoleManager.Create(new IdentityRole("User"));
            }
            if (!RoleManager.RoleExists("SuperAdmin"))
            {
                var roleresult = RoleManager.Create(new IdentityRole("SuperAdmin"));
            }
            if (UserManager.FindByEmail("admin@gmail.com") == null)
            {
                var user = new ApplicationUser();
                user.UserName = "admin@gmail.com";
                user.Email = "admin@gmail.com";
               
                var adminResult = UserManager.Create(user, password);
                if (adminResult.Succeeded)
                {
                    var result = UserManager.AddToRole(user.Id, role);
                }

            }
        }
    }
}
