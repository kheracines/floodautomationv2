﻿var options1 = {
    chart: {
        renderTo: 'container'
    },
    series: []
};

var drawChart = function (data, name) {
    // 'series' is an array of objects with keys: 'name' (string) and 'data' (array)
    var newSeriesData = {
        name: name,
        data: data
    };

    // Add the new data to the series array
    options1.series.push(newSeriesData);

    // If you want to remove old series data, you can do that here too

    // Render the chart
    var chart = new Highcharts.Chart(options1);
};
var data = [];
$.getJSON("/Administration/GetDevices/", function (names) {
    for (var i = 0; i <= names.length - 1; i++) {

        $.getJSON("/Administration/GetDevices/", function (water) {
            for (var e = 0; e <= water.length - 1; e++) {
                if (names[i].Id == water[e].DeviceId) {
                    data = Date.UTC(water[e].DateReceived), water[e].Message;
                    drawChart(data, names[i].NameOfPlace);
                }
            }
        });
    }
});