﻿"use strict";
$(function () {

    var notificationFromHub = $.connection.sMSHub;
    $.connection.hub.start().done(function () {
        FetchData();
    });
    notificationFromHub.client.updatedClients = function () {
        FetchData();
    };

});




function FetchData() {

    $.ajax({
        url: '/Administration/GetChartFlowMeterData',
        contentType: 'application/json ; charset:utf-8',
        type: 'GET',
        dataType: 'json'
    }).success(function (data) {

        var JSON = data;

        var options = {
            chart: {
                renderTo: 'container',
                type: 'spline',
               
            },
            credits: {
                enabled: 0
            },
            title: {
                text: 'Flow Meter in the last 24 Hours'
            },
            subtitle: {
                text: 'North Cotabato, Philippines'
            },
            legend: {
                enabled: true,
                //align: 'left',
                //borderColor: 'white',
                //backgroundColor: 'white',
                //color: 'rgb(255, 0, 0)',
                //borderWidth: 1,
                //layout: 'vertical',
                //verticalAlign: 'top',
                ////x: 190,
                ////y: 90,
                ////floating: true,
                //shadow: true
            },
            navigator: {
                enabled: false
            },
            scrollbar: {
                enabled: false
            },
            rangeSelector: {
                inputEnabled: false,
                selected: 0,
                buttons: [{
                    type: 'day',
                    count: 1,
                    text: '1d'
                }],
                buttonTheme: {
                    visibility: 'hidden'
                },
                labelStyle: {
                    visibility: 'hidden'
                }
            },
            yAxis: {
                max: 10,
                min: 0,
                title: {
                    text: 'Flow Meter  (m^2/S)'
                },
                minorGridLineWidth: 0,
                gridLineWidth: 0,
                alternateGridColor: null,
                plotBands: [{ // Light air
                    from: 0,
                    to: 2,
                    color: '#99e6ff',
                    label: {
                        text: 'Low',
                        style: {
                            color: '#000000'
                        }
                    }
                }, { // Light breeze
                    from: 2,
                    to: 5,
                    color: '#4d94ff',
                    label: {
                        text: 'Normal Level',
                        style: {
                            color: '#000000'
                        }
                    }
                }, { // Gentle breeze
                    from: 5,
                    to: 7,
                    color: '#ffb31a',
                    label: {
                        text: 'High Level',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // Moderate breeze
                    from: 7,
                    to: 10,
                    color: '#ff3300',
                    label: {
                        text: 'very high',
                        style: {
                            color: '#606060'
                        }
                    }
                }, ]
            },
            tooltip: {
                valueSuffix: ' m³/S',
            },
            plotOptions: {
                spline: {
                    dataLabels: {
                        enabled: true,
                        color: 'black'
                    },
                    marker: {
                        enabled: true
                    }
                }
            },

            series: [],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            align: 'center',
                            verticalAlign: 'bottom',
                            layout: 'horizontal'
                        },
                        yAxis: {
                            labels: {
                                align: 'left',
                                x: 0,
                                y: -5
                            },
                            title: {
                                text: null
                            }
                        },
                        subtitle: {
                            text: null
                        },
                        credits: {
                            enabled: false
                        }
                    }
                }]
            },

            navigation: {
                menuItemStyle: {
                    fontSize: '10px'
                }
            }
        };

        options.series = JSON;
        // Create the chart
        var chart = new Highcharts.stockChart(options);


    })
    FetchMessageNotify();

}

