﻿"use strict";
$(function () {

    var notificationFromHub = $.connection.sMSHub;
    $.connection.hub.start().done(function () {
        FetchData();
    });
    notificationFromHub.client.updatedClients = function () {
        FetchData();
    };

});



$(document).ready(function () {
    setInterval(function () {
        var url = '/Administration/Index';
        $.get(url, function (t) {
        })
    }, 1000);
})

function FetchData() {

    $.ajax({
        url: '/Administration/GetChartWaterData',
        contentType: 'application/json ; charset:utf-8',
        type: 'GET',
        dataType: 'json'
    }).success(function (data) {
        
        var JSON = data;

        var options = {
            chart: {
                renderTo: 'container',
                type: 'spline'
            },
            title: {
                text: 'Water Level in the last 24 Hours'
            },
            subtitle: {
                text: 'North Cotabato, Philippines'
            },
            xAxis: {

                labels: {
                    enabled: false

                    //overflow: 'justify',
                }
            },
            yAxis: {
                title: {
                    text: 'Water Level (m)'
                },
                minorGridLineWidth: 0,
                gridLineWidth: 0,
                alternateGridColor: null,
                plotBands: [{ // Light air
                    from: 0.3,
                    to: 1.5,
                    color: 'rgba(68, 170, 213, 0.1)',
                    label: {
                        text: 'A',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // Light breeze
                    from: 1.5,
                    to: 3.3,
                    color: 'rgba(0, 0, 0, 0)',
                    label: {
                        text: 'B',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // Gentle breeze
                    from: 3.3,
                    to: 5.5,
                    color: 'rgba(68, 170, 213, 0.1)',
                    label: {
                        text: 'C',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // Moderate breeze
                    from: 5.5,
                    to: 8,
                    color: 'rgba(0, 0, 0, 0)',
                    label: {
                        text: 'D',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // Fresh breeze
                    from: 8,
                    to: 11,
                    color: 'rgba(68, 170, 213, 0.1)',
                    label: {
                        text: 'E',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // Strong breeze
                    from: 11,
                    to: 14,
                    color: 'rgba(0, 0, 0, 0)',
                    label: {
                        text: 'F',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // High wind
                    from: 14,
                    to: 15,
                    color: 'rgba(68, 170, 213, 0.1)',
                    label: {
                        text: 'G',
                        style: {
                            color: '#606060'
                        }
                    }
                }]
            },
            tooltip: {
                valueSuffix: ' m'
            },
            plotOptions: {
                spline: {
                    marker: {
                        enabled: true
                    }
                }
            },

            series: [],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            align: 'center',
                            verticalAlign: 'bottom',
                            layout: 'horizontal'
                        },
                        yAxis: {
                            labels: {
                                align: 'left',
                                x: 0,
                                y: -5
                            },
                            title: {
                                text: null
                            }
                        },
                        subtitle: {
                            text: null
                        },
                        credits: {
                            enabled: false
                        }
                    }
                }]
            },

            navigation: {
                menuItemStyle: {
                    fontSize: '10px'
                }
            }
        };

        options.series = JSON;
        // Create the chart
        var chart = new Highcharts.Chart(options);



    })

}

