﻿"use strict";
$(function () {

    var notificationFromHub = $.connection.sMSHub;
    $.connection.hub.start().done(function () {
        FetchData();
    });
    notificationFromHub.client.updatedClients = function () {
        FetchData();
    };

});



//$(document).ready(function () {
//    setInterval(function () {
//        var url = '/Administration/Index';
//        $.get(url, function (t) {
//        })
//    }, 1000);
//})

function FetchData() {

    $.ajax({
        url: '/Administration/GetChartRainGaugedData',
        contentType: 'application/json ; charset:utf-8',
        type: 'GET',
        dataType: 'json'
    }).success(function (data) {

        var JSON = data;

        var options = {
            chart: {
                renderTo: 'container',
                type: 'spline',
              
            },
            credits: {
                enabled: 0
            },
            title: {
                text: 'Rainfall in the last 24 Hours'
            },
            subtitle: {
                text: 'North Cotabato, Philippines'
            },
            legend: {
                enabled: true,
            
            },
      
            navigator: {
                enabled: false
            },
            scrollbar: {
                enabled: false
            },
            rangeSelector: {
                inputEnabled: false,
                selected: 0,
                buttons: [{
                    type: 'day',
                    count: 1,
                    text: '1d'
                }],
                buttonTheme: {
                    visibility: 'hidden'
                },
                labelStyle: {
                    visibility: 'hidden'
                }
            },
            yAxis: {
                max: 10,
                min: 0,
                title: {
                    text: 'Rainfall (mm)'
                },

                minorGridLineWidth: 0,
                gridLineWidth: 0,
                alternateGridColor: null,
                plotBands: [{ // Light air
                    from: 0,
                    to: 1,
                    color: '#99e6ff',
                    label: {
                        text: 'Light',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // Light breeze
                    from: 1,
                    to: 3,
                    color: '#4d94ff',
                    label: {
                        text: 'Moderate',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // Gentle breeze
                    from: 3,
                    to: 5,
                    color: '#1a8cff',
                    label: {
                        text: 'Heavy',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // Moderate breeze
                    from: 5,
                    to: 7,
                    color: '#ffb31a',
                    label: {
                        text: 'Intense',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // Fresh breeze
                    from: 7,
                    to: 10,
                    color: '#ff3300',
                    label: {
                        text: 'Torrential',
                        style: {
                            color: '#606060'
                        }
                    }

                }]
            },
            tooltip: {
                valueSuffix: ' mm'
            },
            plotOptions: {
                spline: {
                    dataLabels: {
                        enabled: true,
                        color: 'black'
                    },
                    marker: {
                        enabled: true
                    }
                }
            },

            series: [],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            align: 'center',
                            verticalAlign: 'bottom',
                            layout: 'horizontal'
                        },
                        yAxis: {
                            labels: {
                                align: 'left',
                                x: 0,
                                y: -5
                            },
                            title: {
                                text: null
                            }
                        },
                        subtitle: {
                            text: null
                        },
                        credits: {
                            enabled: false
                        }
                    }
                }]
            },

            navigation: {
                menuItemStyle: {
                    fontSize: '10px'
                }
            }
        };

        options.series = JSON;
        // Create the chart
        var chart = new Highcharts.stockChart(options);



    })
    FetchMessageNotify();

}

