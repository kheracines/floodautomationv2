﻿"use strict";
$(function () {

    var notificationFromHub = $.connection.sMSHub;
    $.connection.hub.start().done(function () {
        FetchData();
    });
    notificationFromHub.client.updatedClients = function () {
        FetchData();
    };

});



function FetchData() {

    var model = $('.data-container');
    $.ajax({
        url: '/Administration/_GetRainGauge',
        contentType: 'application/html ; charset:utf-8',
        type: 'GET',
        dataType: 'html'
    }).success(function (result) {
        model.empty().append(result);
    })
    FetchMessageNotify();

}


$(document).ready(function () {

    //setInterval(function () {
    //    var url = '/Administration/_GetRainGauge';
    //    $.get(url, function (t) {
    //    })
    //}, 1000);

    //$(document).on('click', '.refresh-Gauge', function (e) {
    //    e.preventDefault();
    //    FetchData();
    //})


})
