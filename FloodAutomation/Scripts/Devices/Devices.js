﻿"use strict";

$(document).ready(function () {
    DeviceData.getList();

    $(document).on('click', '.create-device', function (e) {
        e.preventDefault();
        DeviceData.addEdit();
    })

    $(document).on('click', '.edit-device', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var title = $(this).data('title');
        DeviceData.addEdit(id, title);
    })

    $(document).on("click", ".btn-delete-device", function (e) {
        e.preventDefault();
        var ID = $(this).data('id');
        var name = "<b>" + $(this).data("title") + "</b>";
        $.alert({
            icon: 'fa fa-warning',
            title: 'Delete Confirm!',
            animation: 'left',
            closeAnimation: 'scale',
            theme: 'material',
            columnClass: 'col-md-6 col-md-offset-3',
            content: 'Are you sure you want to delete ' + name,
            buttons: {
                confirm: {
                    btnClass: 'btn-red any-other-class',
                    action: function () {
                        $.ajax({
                            url: '/Administration/DeleteDevice/?id=' + ID,
                            type: 'POST',
                            data: JSON,
                            contentType: "application/json;charset=utf-8",
                            success: function (data) {
                                DeviceData.getList();
                                $.alert('Data deleted Successfully');
                            },
                            error: function (x, y, z) {
                                $.alert(x + '\n' + y + '\n' + z);
                            }
                        });
                    }
                },
                cancel: function () {
                },
            }
        });
    })


    $(document).on('click', '.btn-submit-form', function (e) {
        e.preventDefault();

        DeviceData.saved();
    })

})

var DeviceData = {
    getList: function () {


        var url = '/Administration/_GetDevices';
        //$('.data-container').html('Loading data...');

        $.get(url, function (result) {
            $('.data-container').html(result);
        }).done(function () {
            DeviceData.initTable();
        })
    },
    initTable: function () {
        if ($('#Devices').length > 0) {
            $("#Devices").dataTable({
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [-1] }
                ]
            });
        }
    },


    addEdit: function (id, titleCaption) {

        var title = 'Add Device';

        var url = '/Administration/CreateEditDevices';
        if (id) {
            url = '/Administration/CreateEditDevices/?id=' + id;
            title = titleCaption
        }
        $('#myModal').modal('show');
        $('#myModal .modal-title').text(title);
        $.get(url, function (result) {
            $('#myModal .modal-body').html(result);
        }).done(function () {
            utilities.JQValidateParse('#createEdit-device-form');
        })
    },

    saved: function () {

        var form = $('#createEdit-device-form');

        form.validate();
        if (form.valid()) {
            utilities.form.save({
                form: form,
                success: function (data, textStatus, xhr) {
                    switch (data.NotifyType) {
                        case 0: $(".response-div").html(data.Html);
                            break;
                    }

                    if (data.Success) {
                        setTimeout(function () {
                            $(".response-div").html('');
                            $('#myModal').modal('hide');
                            DeviceData.getList();
                        }, 2000)
                    }
                },
                error: function () {
                    form.find(".response-div").html(utilities.error());
                }
            })

        }
    }
}