﻿"use strict";
$(function () {

    var notificationFromHub = $.connection.sMSHub;
    $.connection.hub.start().done(function () {
        FetchData();
    });
    notificationFromHub.client.updatedClients = function () {
        FetchData();
    };

});



//$(document).ready(function () {
//    setInterval(function () {
//        var url = '/Administration/Index';
//        $.get(url, function (t) {
//        })
//    }, 1000);
//})

function FetchData() {

    $.ajax({
        url: '/Administration/GetChartRainGaugedDataHistory',
        contentType: 'application/json ; charset:utf-8',
        type: 'GET',
        dataType: 'json'
    }).success(function (data) {


        var JSON = data;
        var options = {
            chart: {
                renderTo: 'container',
                type: 'spline',
                zoomType: 'x'
            },
            global: {
                useUTC: false
            },
            credits: {
                enabled: 0
            },
            title: {
                text: 'Rain Gauged Level History'
            },
            subtitle: {
                text: 'North Cotabato, Philippines'
            },
            legend: {
                enabled: true,
                //align: 'left',
                //borderColor: 'white',
                //backgroundColor: 'black',

                //color: 'rgb(255, 0, 0)',
                //borderWidth: 1,
                //layout: 'vertical',
                //verticalAlign: 'top',
                //x: 190,
                //y: 90,
                //floating: true,
                //shadow: true
            },
            rangeSelector: {

                buttons: [{
                    type: 'day',
                    count: 3,
                    text: '3d'
                }, {
                    type: 'week',
                    count: 1,
                    text: '1w'
                }, {
                    type: 'month',
                    count: 1,
                    text: '1m'
                }, {
                    type: 'month',
                    count: 6,
                    text: '6m'
                }, {
                    type: 'year',
                    count: 1,
                    text: '1y'
                }, {
                    type: 'all',
                    text: 'All'
                }],
                selected: 3
            },



            yAxis: {
                max: 10,
                min: 0,
                title: {
                    text: 'Rainfall (mm)'
                },

                minorGridLineWidth: 0,
                gridLineWidth: 0,
                alternateGridColor: null,
                plotBands: [{ // Light air
                    from: 0,
                    to: 1,
                    color: '#99e6ff',
                    label: {
                        text: 'Light',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // Light breeze
                    from: 1,
                    to: 3,
                    color: '#4d94ff',
                    label: {
                        text: 'Moderate',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // Gentle breeze
                    from: 3,
                    to: 5,
                    color: '#1a8cff',
                    label: {
                        text: 'Heavy',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // Moderate breeze
                    from: 5,
                    to: 7,
                    color: '#ffb31a',
                    label: {
                        text: 'Intense',
                        style: {
                            color: '#606060'
                        }
                    }
                }, { // Fresh breeze
                    from: 7,
                    to: 10,
                    color: '#ff3300',
                    label: {
                        text: 'Torrential',
                        style: {
                            color: '#606060'
                        }
                    }

                }]
            },
            tooltip: {
                valueSuffix: ' mm'
            },
            plotOptions: {
                spline: {
                    dataLabels: {
                        enabled: true,
                        color: 'black'
                    },
                    marker: {
                        enabled: true
                    }
                }
            },

            series: [],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            align: 'center',
                            verticalAlign: 'bottom',
                            layout: 'horizontal'
                        },
                        yAxis: {
                            labels: {
                                align: 'left',
                                x: 0,
                                y: -5
                            },
                            title: {
                                text: null
                            }
                        },
                        subtitle: {
                            text: null
                        },
                        credits: {
                            enabled: false
                        }
                    }
                }]
            },

            navigation: {
                menuItemStyle: {
                    fontSize: '10px'
                }
            }
        };
        options.series = JSON;
        // Create the chart
        var chart = new Highcharts.stockChart(options);



    })
    FetchMessageNotify();

}

var start = moment().subtract(29, 'days');
var end = moment();


$(function () {
    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        opens: 'right',
        startDate: start,
        endDate: end,
    }, cb);

    cb(start, end);

});

function printDiv(divID) {
    //Get the HTML of div
    var divElements = document.getElementById(divID).innerHTML;
    //Get the HTML of whole page
    var oldPage = document.body.innerHTML;

    //Reset the page's HTML with div's HTML only
    document.body.innerHTML =
      "<html><head><title>Water Level</title></head><body>" +
      divElements + "</body>";

    //Print Page
    window.print();

    //Restore orignal HTML
    document.body.innerHTML = oldPage;
}
$(document).ready(function () {


    $(document).on('click', 'button.cancel', function (e) {
        e.preventDefault();
        window.close();
    })


    $(document).on('click', 'button.applyBtn', function (e) {
        e.preventDefault();
        var startdate = $('#reportrange').data('daterangepicker').startDate;
        var enddate = $('#reportrange').data('daterangepicker').endDate;
        var startdates = (startdate.format('MMMM D, YYYY'));
        var enddates = (enddate.format('MMMM D, YYYY'));
        var id = startdates + "-" + enddates;

        RainGaugedData.getList(id);
        $(".print-rain").removeClass("disabled");
    })
})

var RainGaugedData = {
    getList: function (id) {
        var url = '/Administration/PrintRainGaugedDataHistory?id=' + id;
        $.get(url, function (result) {
            $('#tableContainer').html(result);
        }).done(function () {
            $("#RainGauged").dataTable({
                dom: 'lBfrtip',
                buttons: [
                    'print',

                ],

                "order": [[0, "desc"]],
                "iDisplayLength": 100,
            });
        })
    },
}