﻿"use strict";
$(function () {

    var notificationFromHub = $.connection.messageHub;
    $.connection.hub.start().done(function () {
        FetchMessage();
    });
    notificationFromHub.client.updatedClients = function () {
        FetchMessage();
    };

});



function FetchMessage() {

    var model = $('.data-container');
    $.ajax({
        url: '/Administration/GetAllMessagesNotification',
        contentType: 'application/html ; charset:utf-8',
        type: 'GET',
        dataType: 'html'
    }).success(function (result) {
        model.empty().append(result);
    })
    FetchMessageNotify();

}


