﻿"use strict";
$(function () {

    var notificationFromHub = $.connection.messageHub;
    $.connection.hub.start().done(function () {
        FetchUser();
    });
    notificationFromHub.client.updatedClients = function () {
        FetchUser();
    };

});



function FetchUser() {

    var model = $('.data-container');
    $.ajax({
        url: '/Administration/GetAllUserLog',
        contentType: 'application/html ; charset:utf-8',
        type: 'GET',
        dataType: 'html'
    }).success(function (result) {
        model.empty().append(result);
    })
    FetchUserNotify();
}


