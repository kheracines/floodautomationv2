﻿"use strict";


$(function () {

    var notificationFromHub = $.connection.messageHub;
    $.connection.hub.start().done(function () {
        CounterDevices();
        CounterUsersReg();
        CounterMessage();
        CounterUsers();
       
    });
    notificationFromHub.client.updatedClients = function () {
        CounterDevices();
        CounterMessage();
        CounterUsersReg();
        CounterUsers();
        FetchUserNotify();
        FetchMessageNotify();
    };

});

function CounterDevices() {
    var model = $('.Devicescounter');
    $.ajax({
        url: '/Administration/CounterDevices',
        contentType: 'application/html ; charset:utf-8',
        type: 'GET',
        dataType: 'html'
    }).success(function (result) {
        model.empty().append(result);
    })
}
function CounterUsersReg() {
    var model = $('.userregcounter');
    $.ajax({
        url: '/Administration/CounterRegUsers',
        contentType: 'application/html ; charset:utf-8',
        type: 'GET',
        dataType: 'html'
    }).success(function (result) {
        model.empty().append(result);
    })
}

function CounterUsers() {
    var model = $('.counterusers');
    $.ajax({
        url: '/Administration/UserCounter',
        contentType: 'application/html ; charset:utf-8',
        type: 'GET',
        dataType: 'html'
    }).success(function (result) {
        model.empty().append(result);
    })
}
function CounterMessage() {
    var model = $('.countermessage');
    $.ajax({
        url: '/Administration/MessageCounter',
        contentType: 'application/html ; charset:utf-8',
        type: 'GET',
        dataType: 'html'
    }).success(function (result) {
        model.empty().append(result);
    })
}
