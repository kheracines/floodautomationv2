﻿"use strict";

$(document).ready(function () {
    USERData.getList();

    $(document).on('click', '.create-USER', function (e) {
        e.preventDefault();
        USERData.addEdit();
    })

    $(document).on('click', '.edit-Users', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var title = $(this).data('title');
        USERData.addEdit(id, title);
    })

    $(document).on('click', '.btn-submit-form', function (e) {
        e.preventDefault();
        USERData.saveUsers();
    })

    $(document).on('focusout', '#Email', function (e) {
        e.preventDefault();
        var email = $(this).val();
        USERData.checkEmail(email);

    })

})

var USERData = {
    checkEmail: function (email) {
        var actionUrl = "/Administration/?email=" + email;

        $.ajax({
            url: actionUrl,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                if (data.result) {

                    $('.checkEmailValidationContainer').text("Email exists!").css("display", "block");
                }
                else {
                    $('.checkEmailValidationContainer').css("display", "none");
                }
            }
        });
    },

    getList: function () {


        var url = '/Administration/GetUserList';
        //$('.data-container').html('Loading data...');

        $.get(url, function (result) {
            $('.data-container').html(result);
        }).done(function () {
            USERData.initTable();
        })
    },
    initTable: function () {
        if ($('#USERTable').length > 0) {
            $("#USERTable").dataTable({
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [-1] }
                ]
            });
        }
    },

 
    initTable: function () {
        if ($('#USERTable').length > 0) {
            $("#USERTable").dataTable({
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [-1] }
                ]
            });
        }
    },
    addEdit: function (id, titleCaption) {

        var title = 'Add Users';

        var url = '/Administration/CreateEditUsers';
        if (id) {
            url = '/Administration/CreateEditUsers/?id=' + id;
            title = titleCaption
        }
        $('#myModal').modal('show');
        $('#myModal .modal-title').text(title);
        $.get(url, function (result) {
            $('#myModal .modal-body').html(result);
        }).done(function () {
            utilities.JQValidateParse('#user-addedit-form');
        })
    },
    saveUsers: function () {

        var form = $('#user-addedit-form');

        form.validate();
        if (form.valid()) {
            utilities.form.save({
                form: form,
                success: function (data, textStatus, xhr) {
                    switch (data.NotifyType) {
                        case 0: $(".response-div").html(data.Html);
                            break;
                    }

                    if (data.Success) {
                        setTimeout(function () {
                            $(".response-div").html('');
                            $('#myModal').modal('hide');
                            USERData.getList();
                        }, 2000)
                    }
                    else {
                        var email = $('#Email').val();
                        USERData.checkEmail(email);
                    }
                },
                error: function () {
                    form.find(".response-div").html(utilities.error());
                }
            })

        }
    }
}