﻿"use strict";

var water;
var rain;
var flow;
var markerWater;
var markerRain;
var markerFlow;
var Id;
var title;
var count = 0;
$(document).ready(function () {
    Initialize();
});

var ViewArea = {
    ViewWater: function (Id, title) {
        $('#myModal').modal('show');
        $('#myModal .modal-title').text(title);

        $.getJSON("/Home/WaterGetGMAPDevices/?Id=" + Id, function (data) {
            var JSON = data;

            var options = {
                chart: {
                    renderTo: 'modal-body',
                    type: 'spline'
                },
                credits: {
                    enabled: 0
                },
                title: {
                    text: 'Water Level in the last 24 Hours'
                },
                subtitle: {
                    text: 'North Cotabato, Philippines'
                },
                xAxis: {
                    labels: {
                        enabled: false

                        //overflow: 'justify',
                    }
                },
                yAxis: {
                    max: 10,
                    min: 0,
                    title: {
                        text: 'Water Level  (m)'
                    },
                    minorGridLineWidth: 0,
                    gridLineWidth: 0,
                    alternateGridColor: null,
                    plotBands: [{ // Light air
                        from: 0,
                        to: 2,
                        color: '#99e6ff',
                        label: {
                            text: 'Low',
                            style: {
                                color: '#000000'
                            }
                        }
                    }, { // Light breeze
                        from: 2,
                        to: 5,
                        color: '#4d94ff',
                        label: {
                            text: 'Normal Level',
                            style: {
                                color: '#000000'
                            }
                        }
                    }, { // Gentle breeze
                        from: 5,
                        to: 7,
                        color: '#ffb31a',
                        label: {
                            text: 'High Level',
                            style: {
                                color: '#606060'
                            }
                        }
                    }, { // Moderate breeze
                        from: 7,
                        to: 10,
                        color: '#ff3300',
                        label: {
                            text: 'very high',
                            style: {
                                color: '#606060'
                            }
                        }
                    }, ]
                },
                tooltip: {

                    valueSuffix: ' m'
                },
                plotOptions: {
                    spline: {
                        dataLabels: {
                            enabled: true,
                            color: 'black'
                        },
                        marker: {
                            enabled: true
                        }
                    }
                },

                series: [],

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                align: 'center',
                                verticalAlign: 'bottom',
                                layout: 'horizontal'
                            },
                            yAxis: {
                                labels: {
                                    align: 'left',
                                    x: 0,
                                    y: -5
                                },
                                title: {
                                    text: null
                                }
                            },
                            subtitle: {
                                text: null
                            },
                            credits: {
                                enabled: false
                            }
                        }
                    }]
                },

                navigation: {
                    menuItemStyle: {
                        fontSize: '10px'
                    }
                }
            };

            options.series = JSON;
            // Create the chart
            var chart = new Highcharts.Chart(options);



        })

    },
    ViewFlow: function (Id, title) {
        $('#myModal').modal('show');
        $('#myModal .modal-title').text(title);
        $.getJSON("/Home/FlowGetGMAPDevices/?Id=" + Id, function (data) {
            var JSON = data;

            var options = {
                chart: {
                    renderTo: 'modal-body',
                    type: 'spline',

                },
                credits: {
                    enabled: 0
                },
                title: {
                    text: 'Flow Meter in the last 24 Hours'
                },
                subtitle: {
                    text: 'North Cotabato, Philippines'
                },
                xAxis: {
                    labels: {
                        enabled: false

                        //overflow: 'justify',
                    }
                },
                yAxis: {
                    max: 10,
                    min: 0,
                    title: {
                        text: 'Flow Meter  (m)'
                    },
                    minorGridLineWidth: 0,
                    gridLineWidth: 0,
                    alternateGridColor: null,
                    plotBands: [{ // Light air
                        from: 0,
                        to: 2,
                        color: '#99e6ff',
                        label: {
                            text: 'Low',
                            style: {
                                color: '#000000'
                            }
                        }
                    }, { // Light breeze
                        from: 2,
                        to: 5,
                        color: '#4d94ff',
                        label: {
                            text: 'Normal Level',
                            style: {
                                color: '#000000'
                            }
                        }
                    }, { // Gentle breeze
                        from: 5,
                        to: 7,
                        color: '#ffb31a',
                        label: {
                            text: 'High Level',
                            style: {
                                color: '#606060'
                            }
                        }
                    }, { // Moderate breeze
                        from: 7,
                        to: 10,
                        color: '#ff3300',
                        label: {
                            text: 'very high',
                            style: {
                                color: '#606060'
                            }
                        }
                    }, ]
                },
                tooltip: {
                    valueSuffix: ' m³/S',
                },
                plotOptions: {
                    spline: {
                        dataLabels: {
                            enabled: true,
                            color: 'black'
                        },
                        marker: {
                            enabled: true
                        }
                    }
                },

                series: [],

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                align: 'center',
                                verticalAlign: 'bottom',
                                layout: 'horizontal'
                            },
                            yAxis: {
                                labels: {
                                    align: 'left',
                                    x: 0,
                                    y: -5
                                },
                                title: {
                                    text: null
                                }
                            },
                            subtitle: {
                                text: null
                            },
                            credits: {
                                enabled: false
                            }
                        }
                    }]
                },

                navigation: {
                    menuItemStyle: {
                        fontSize: '10px'
                    }
                }
            };

            options.series = JSON;
            // Create the chart
            var chart = new Highcharts.Chart(options);



        })
    },
    ViewRain: function (Id, title) {
        $('#myModal').modal('show');
        $('#myModal .modal-title').text(title);
        $.getJSON("/Home/RainGetGMAPDevices/?Id=" + Id, function (data) {
            var JSON = data;

            var options = {
                chart: {
                    renderTo: 'modal-body',
                    type: 'spline',
                    data: {
                        table: 'datatable'
                    },
                },
                credits: {
                    enabled: 0
                },
                title: {
                    text: 'Rainfall in the last 24 Hours'
                },
                subtitle: {
                    text: 'North Cotabato, Philippines'
                },
                xAxis: {
                    labels: {
                        enabled: false

                        //overflow: 'justify',
                    }
                },
                yAxis: {
                    max: 10,
                    min: 0,
                    title: {
                        text: 'Rainfall (mm)'
                    },

                    minorGridLineWidth: 0,
                    gridLineWidth: 0,
                    alternateGridColor: null,
                    plotBands: [{ // Light air
                        from: 0,
                        to: 1,
                        color: '#99e6ff',
                        label: {
                            text: 'Light',
                            style: {
                                color: '#606060'
                            }
                        }
                    }, { // Light breeze
                        from: 1,
                        to: 3,
                        color: '#4d94ff',
                        label: {
                            text: 'Moderate',
                            style: {
                                color: '#606060'
                            }
                        }
                    }, { // Gentle breeze
                        from: 3,
                        to: 5,
                        color: '#1a8cff',
                        label: {
                            text: 'Heavy',
                            style: {
                                color: '#606060'
                            }
                        }
                    }, { // Moderate breeze
                        from: 5,
                        to: 7,
                        color: '#ffb31a',
                        label: {
                            text: 'Intense',
                            style: {
                                color: '#606060'
                            }
                        }
                    }, { // Fresh breeze
                        from: 7,
                        to: 10,
                        color: '#ff3300',
                        label: {
                            text: 'Torrential',
                            style: {
                                color: '#606060'
                            }
                        }

                    }]
                },
                tooltip: {
                    valueSuffix: ' mm'
                },
                plotOptions: {
                    spline: {
                        dataLabels: {
                            enabled: true,
                            color: 'black'
                        },
                        marker: {
                            enabled: true
                        }
                    }
                },

                series: [],

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                align: 'center',
                                verticalAlign: 'bottom',
                                layout: 'horizontal'
                            },
                            yAxis: {
                                labels: {
                                    align: 'left',
                                    x: 0,
                                    y: -5
                                },
                                title: {
                                    text: null
                                }
                            },
                            subtitle: {
                                text: null
                            },
                            credits: {
                                enabled: false
                            }
                        }
                    }]
                },

                navigation: {
                    menuItemStyle: {
                        fontSize: '10px'
                    }
                }
            };

            options.series = JSON;
            // Create the chart
            var chart = new Highcharts.Chart(options);



        })
    },
}



// Where all the fun happens
function Initialize() {

    var styledMapType = new google.maps.StyledMapType(
        [
         {
             "featureType": "administrative.neighborhood",
             "elementType": "labels",
             "stylers": [{
                 "visibility": "off"
             }]
         }, {
             "featureType": "administrative.land_parcel",
             "elementType": "labels",
             "stylers": [{
                 "visibility": "off"
             }]
         }, {
             "featureType": "administrative.locality",
             "elementType": "labels",
             "stylers": [{
                 "visibility": "off"
             }]
         },
        ],
 { name: 'Styled Map' });


    // Google has tweaked their interface somewhat - this tells the api to use that new UI
    google.maps.visualRefresh = true;
    var NorthCotabato = new google.maps.LatLng(7.1083, 125.0388);

    // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
    var mapOptions = {
        zoom: 9,
        center: NorthCotabato,
        mapTypeId: 'terrain',
        draggable: true,
        zoomControl: false,
        scrollwheel: true,
        disableDoubleClickZoom: true,

    };
    var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

    //Associate the styled map with the MapTypeId and set it to display.
    //map.mapTypes.set('styled_map', styledMapType);
    //map.setMapTypeId('styled_map');
    // This makes the div with id "map_canvas" a google map

    // a sample list of JSON encoded data of places to visit in Tunisia
    // you can either make up a JSON list server side, or call it from a controller using JSONResult
    //124.678774 6.687966
    $.getJSON("/Home/GetDevicesBack/", function (names) {

        // Using the JQuery "each" selector to iterate through the JSON list and drop marker pins
        $.each(names, function (i, item) {

            if (item.Category == 1) {
                markerRain = new google.maps.Marker({
                    'position': new google.maps.LatLng(item.GeoLat, item.GeoLong),
                    'map': map,
                    animation: google.maps.Animation.DROP,
                    draggable: true,
                    icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
                    //'label': "test",
                    'title': item.PlaceName,
                })


                google.maps.event.addListener(markerRain, 'click', function () {
                    Id = item.Id;
                    title = item.PlaceName;
                    ViewArea.ViewRain(Id, title);


                });
            }

            if (item.Category == 2) {
                markerFlow = new google.maps.Marker({
                    'position': new google.maps.LatLng(item.GeoLat, item.GeoLong),
                    'map': map,
                    draggable: true,
                    animation: google.maps.Animation.DROP,
                    icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png',

                    //'label': "test",
                    'title': item.PlaceName,
                });

                google.maps.event.addListener(markerFlow, 'click', function () {
                    Id = item.Id;
                    title = item.PlaceName;
                    ViewArea.ViewFlow(Id, title);
                });
            }
            if (item.Category == 3) {

                markerWater = new google.maps.Marker({
                    'position': new google.maps.LatLng(item.GeoLat, item.GeoLong),
                    'map': map,
                    draggable: true,
                    //opacity: 0.7,
                    animation: google.maps.Animation.DROP,
                    //'label': "test",
                    icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
                    'title': item.PlaceName,
                    scaledSize: new google.maps.Size(71, 71),

                });

                google.maps.event.addListener(markerWater, 'click', function () {
                    Id = item.Id;
                    title = item.PlaceName;
                    ViewArea.ViewWater(Id, title);
                });

            }
     
            
            //markerWater.setVisible(true);
            //markerWater.setMap(null);      // visible_changed not triggered
            //markerWater.setMap(map);
            //markerRain.setVisible(false);

            // finally hook up an "OnClick" listener to the map so it pops up out info-window when the marker-pin is clicked!


        })
        //}
    })

}

function WaterCheck() {


        var styledMapType = new google.maps.StyledMapType(
            [
             {
                 "featureType": "administrative.neighborhood",
                 "elementType": "labels",
                 "stylers": [{
                     "visibility": "off"
                 }]
             }, {
                 "featureType": "administrative.land_parcel",
                 "elementType": "labels",
                 "stylers": [{
                     "visibility": "off"
                 }]
             }, {
                 "featureType": "administrative.locality",
                 "elementType": "labels",
                 "stylers": [{
                     "visibility": "off"
                 }]
             },
            ],
     { name: 'Styled Map' });


        // Google has tweaked their interface somewhat - this tells the api to use that new UI
        google.maps.visualRefresh = true;
        var NorthCotabato = new google.maps.LatLng(7.1083, 125.0388);

        // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
        var mapOptions = {
            zoom: 9,
            center: NorthCotabato,
            mapTypeId: 'terrain',
            draggable: true,
            zoomControl: false,
            scrollwheel: true,
            disableDoubleClickZoom: true,

        };
        var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

        //Associate the styled map with the MapTypeId and set it to display.
        //map.mapTypes.set('styled_map', styledMapType);
        //map.setMapTypeId('styled_map');
        // This makes the div with id "map_canvas" a google map

        // a sample list of JSON encoded data of places to visit in Tunisia
        // you can either make up a JSON list server side, or call it from a controller using JSONResult
        //124.678774 6.687966
        $.getJSON("/Home/GetDevicesBack/", function (names) {

            // Using the JQuery "each" selector to iterate through the JSON list and drop marker pins
            $.each(names, function (i, item) {

         
                if (item.Category == 3) {

                    markerWater = new google.maps.Marker({
                        'position': new google.maps.LatLng(item.GeoLat, item.GeoLong),
                        'map': map,
                        draggable: true,
                        //opacity: 0.7,
                        animation: google.maps.Animation.DROP,
                        //'label': "test",
                        icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
                        'title': item.PlaceName,
                        scaledSize: new google.maps.Size(71, 71),

                    });

                    google.maps.event.addListener(markerWater, 'click', function () {
                        Id = item.Id;
                        title = item.PlaceName;
                        ViewArea.ViewWater(Id, title);
                    });

                }


                //markerWater.setVisible(true);
                //markerWater.setMap(null);      // visible_changed not triggered
                //markerWater.setMap(map);
                //markerRain.setVisible(false);

                // finally hook up an "OnClick" listener to the map so it pops up out info-window when the marker-pin is clicked!


            })
            //}
        })


}
function FlowCheck() {


        var styledMapType = new google.maps.StyledMapType(
            [
             {
                 "featureType": "administrative.neighborhood",
                 "elementType": "labels",
                 "stylers": [{
                     "visibility": "off"
                 }]
             }, {
                 "featureType": "administrative.land_parcel",
                 "elementType": "labels",
                 "stylers": [{
                     "visibility": "off"
                 }]
             }, {
                 "featureType": "administrative.locality",
                 "elementType": "labels",
                 "stylers": [{
                     "visibility": "off"
                 }]
             },
            ],
     { name: 'Styled Map' });


        // Google has tweaked their interface somewhat - this tells the api to use that new UI
        google.maps.visualRefresh = true;
        var NorthCotabato = new google.maps.LatLng(7.1083, 125.0388);

        // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
        var mapOptions = {
            zoom: 9,
            center: NorthCotabato,
            mapTypeId: 'terrain',
            draggable: true,
            zoomControl: false,
            scrollwheel: true,
            disableDoubleClickZoom: true,

        };
        var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

        //Associate the styled map with the MapTypeId and set it to display.
        //map.mapTypes.set('styled_map', styledMapType);
        //map.setMapTypeId('styled_map');
        // This makes the div with id "map_canvas" a google map

        // a sample list of JSON encoded data of places to visit in Tunisia
        // you can either make up a JSON list server side, or call it from a controller using JSONResult
        //124.678774 6.687966
        $.getJSON("/Home/GetDevicesBack/", function (names) {

            // Using the JQuery "each" selector to iterate through the JSON list and drop marker pins
            $.each(names, function (i, item) {

             

                if (item.Category == 2) {
                    markerFlow = new google.maps.Marker({
                        'position': new google.maps.LatLng(item.GeoLat, item.GeoLong),
                        'map': map,
                        draggable: true,
                        animation: google.maps.Animation.DROP,
                        icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png',

                        //'label': "test",
                        'title': item.PlaceName,
                    });

                    google.maps.event.addListener(markerFlow, 'click', function () {
                        Id = item.Id;
                        title = item.PlaceName;
                        ViewArea.ViewFlow(Id, title);
                    });
                }
              




            })
        })

    
}
function RainCheck() {
    

        var styledMapType = new google.maps.StyledMapType(
            [
             {
                 "featureType": "administrative.neighborhood",
                 "elementType": "labels",
                 "stylers": [{
                     "visibility": "off"
                 }]
             }, {
                 "featureType": "administrative.land_parcel",
                 "elementType": "labels",
                 "stylers": [{
                     "visibility": "off"
                 }]
             }, {
                 "featureType": "administrative.locality",
                 "elementType": "labels",
                 "stylers": [{
                     "visibility": "off"
                 }]
             },
            ],
     { name: 'Styled Map' });


        // Google has tweaked their interface somewhat - this tells the api to use that new UI
        google.maps.visualRefresh = true;
        var NorthCotabato = new google.maps.LatLng(7.1083, 125.0388);

        // These are options that set initial zoom level, where the map is centered globally to start, and the type of map to show
        var mapOptions = {
            zoom: 9,
            center: NorthCotabato,
            mapTypeId: 'terrain',
            draggable: true,
            zoomControl: false,
            scrollwheel: true,
            disableDoubleClickZoom: true,

        };
        var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

        //Associate the styled map with the MapTypeId and set it to display.
        //map.mapTypes.set('styled_map', styledMapType);
        //map.setMapTypeId('styled_map');
        // This makes the div with id "map_canvas" a google map

        // a sample list of JSON encoded data of places to visit in Tunisia
        // you can either make up a JSON list server side, or call it from a controller using JSONResult
        //124.678774 6.687966
        $.getJSON("/Home/GetDevicesBack/", function (names) {

            // Using the JQuery "each" selector to iterate through the JSON list and drop marker pins
            $.each(names, function (i, item) {

                if (item.Category == 1) {
                    markerRain = new google.maps.Marker({
                        'position': new google.maps.LatLng(item.GeoLat, item.GeoLong),
                        'map': map,
                        animation: google.maps.Animation.DROP,
                        draggable: true,
                        icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
                        //'label': "test",
                        'title': item.PlaceName,
                    })


                    google.maps.event.addListener(markerRain, 'click', function () {
                        Id = item.Id;
                        title = item.PlaceName;
                        ViewArea.ViewRain(Id, title);


                    });
                }

      


                //markerWater.setVisible(true);
                //markerWater.setMap(null);      // visible_changed not triggered
                //markerWater.setMap(map);
                //markerRain.setVisible(false);

                // finally hook up an "OnClick" listener to the map so it pops up out info-window when the marker-pin is clicked!


            })
            //}
        })

    
}
function All() {
    Initialize();
}