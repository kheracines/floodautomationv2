﻿"use strict";

$(document).ready(function () {
    RainGauge.getList();

    $(document).on('click', '.create-device', function (e) {
        e.preventDefault();
        RainGauge.addEdit();
    })
    $(document).on('click', '.refresh-Gauge', function (e) {
        e.preventDefault();
        RainGauge.getList();
    })

    $(document).on('click', '.edit-device', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var title = $(this).data('title');
        RainGauge.addEdit(id, title);
    })

    $(document).on('click', '.btn-submit-form', function (e) {
        e.preventDefault();

        RainGauge.saved();
    })

})

var RainGauge = {
    getList: function () {


        var url = '/Administration/_GetRainGauge';
        $('.data-container').html('Loading data...');

        $.get(url, function (result) {
            $('.data-container').html(result);
        }).done(function () {
            RainGauge.initTable();
        })
    },
    initTable: function () {
        if ($('#RainGauge').length > 0) {
            $("#RainGauge").dataTable({
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [-1] }
                ]
            });
        }
    },

    addEdit: function (id, titleCaption) {

        var title = 'Add Device';

        var url = '/Administration/CreateEditDevices';
        if (id) {
            url = '/Administration/CreateEditDevices/?id=' + id;
            title = titleCaption
        }
        $('#myModal').modal('show');
        $('#myModal .modal-title').text(title);
        $.get(url, function (result) {
            $('#myModal .modal-body').html(result);
        }).done(function () {
            utilities.JQValidateParse('#createEdit-device-form');
        })
    },
    saved: function () {

        var form = $('#createEdit-device-form');

        form.validate();
        if (form.valid()) {
            utilities.form.save({
                form: form,
                success: function (data, textStatus, xhr) {
                    switch (data.NotifyType) {
                        case 0: $(".response-div").html(data.Html);
                            break;
                    }

                    if (data.Success) {
                        setTimeout(function () {
                            $(".response-div").html('');
                            $('#myModal').modal('hide');
                            RainGauge.getList();
                        }, 2000)
                    }
                },
                error: function () {
                    form.find(".response-div").html(utilities.error());
                }
            })

        }
    }
}