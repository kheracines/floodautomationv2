﻿"use strict";

$(document).ready(function () {
    WaterLevel.getList();

    $(document).on('click', '.create-device', function (e) {
        e.preventDefault();
        WaterLevel.addEdit();
    })
    $(document).on('click', '.refresh-Level', function (e) {
        e.preventDefault();
        WaterLevel.getList();
    })

    $(document).on('click', '.edit-device', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var title = $(this).data('title');
        WaterLevel.addEdit(id, title);
    })

    $(document).on('click', '.btn-submit-form', function (e) {
        e.preventDefault();

        WaterLevel.saved();
    })

})

var WaterLevel = {
    getList: function () {


        var url = '/Administration/_GetWaterLevel';
        $('.data-container').html('Loading data...');

        $.get(url, function (result) {
            $('.data-container').html(result);
        }).done(function () {
            WaterLevel.initTable();
        })
    },
    initTable: function () {
        if ($('#WaterLevel').length > 0) {
            $("#WaterLevel").dataTable({
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [-1] }
                ]
            });
        }
    },

    addEdit: function (id, titleCaption) {

        var title = 'Add Device';

        var url = '/Administration/CreateEditDevices';
        if (id) {
            url = '/Administration/CreateEditDevices/?id=' + id;
            title = titleCaption
        }
        $('#myModal').modal('show');
        $('#myModal .modal-title').text(title);
        $.get(url, function (result) {
            $('#myModal .modal-body').html(result);
        }).done(function () {
            utilities.JQValidateParse('#createEdit-device-form');
        })
    },
    saved: function () {

        var form = $('#createEdit-device-form');

        form.validate();
        if (form.valid()) {
            utilities.form.save({
                form: form,
                success: function (data, textStatus, xhr) {
                    switch (data.NotifyType) {
                        case 0: $(".response-div").html(data.Html);
                            break;
                    }

                    if (data.Success) {
                        setTimeout(function () {
                            $(".response-div").html('');
                            $('#myModal').modal('hide');
                            WaterLevel.getList();
                        }, 2000)
                    }
                },
                error: function () {
                    form.find(".response-div").html(utilities.error());
                }
            })

        }
    }
}