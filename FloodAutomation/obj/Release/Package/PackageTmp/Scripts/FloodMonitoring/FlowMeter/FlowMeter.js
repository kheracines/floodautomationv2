﻿"use strict";

$(document).ready(function () {
    FlowMeter.getList();

    $(document).on('click', '.refresh-Meter', function (e) {
        e.preventDefault();
        FlowMeter.getList();
    })

    $(document).on('click', '.edit-device', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var title = $(this).data('title');
        FlowMeter.addEdit(id, title);
    })

    $(document).on('click', '.btn-submit-form', function (e) {
        e.preventDefault();

        FlowMeter.saved();
    })

})

var FlowMeter = {
    getList: function () {


        var url = '/Administration/_GetFlowMeter';
        $('.data-container').html('Loading data...');

        $.get(url, function (result) {
            $('.data-container').html(result);
        }).done(function () {
            FlowMeter.initTable();
        })
    },
    initTable: function () {
        if ($('#FlowMeter').length > 0) {
            $("#FlowMeter").dataTable({
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [-1] }
                ]
            });
        }
    },

    addEdit: function (id, titleCaption) {

        var title = 'Add Device';

        var url = '/Administration/CreateEditDevices';
        if (id) {
            url = '/Administration/CreateEditDevices/?id=' + id;
            title = titleCaption
        }
        $('#myModal').modal('show');
        $('#myModal .modal-title').text(title);
        $.get(url, function (result) {
            $('#myModal .modal-body').html(result);
        }).done(function () {
            utilities.JQValidateParse('#createEdit-device-form');
        })
    },
    saved: function () {

        var form = $('#createEdit-device-form');

        form.validate();
        if (form.valid()) {
            utilities.form.save({
                form: form,
                success: function (data, textStatus, xhr) {
                    switch (data.NotifyType) {
                        case 0: $(".response-div").html(data.Html);
                            break;
                    }

                    if (data.Success) {
                        setTimeout(function () {
                            $(".response-div").html('');
                            $('#myModal').modal('hide');
                            FlowMeter.getList();
                        }, 2000)
                    }
                },
                error: function () {
                    form.find(".response-div").html(utilities.error());
                }
            })

        }
    }
}