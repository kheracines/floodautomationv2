﻿"use strict";

$(document).ready(function () {
    USERData.getList();

    $(document).on('click', '.create-sms', function (e) {
        e.preventDefault();
        USERData.addEdit();
    })

    $(document).on('click', '.edit-sms', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var title = $(this).data('title');
        USERData.addEdit(id, title);
    })

    $(document).on('click', '.btn-submit-form', function (e) {
        e.preventDefault();

        USERData.saveCollege();
    })

})

var USERData = {
    getList: function () {


        var url = '/Administration/_GetUsers';
        $('.data-container').html('Loading data...');

        $.get(url, function (result) {
            $('.data-container').html(result);
        }).done(function () {
            USERData.initTable();
        })
    },
    initTable: function () {
        if ($('#USERTable').length > 0) {
            $("#USERTable").dataTable({
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [-1] }
                ]
            });
        }
    },

    addEdit: function (id, titleCaption) {

        var title = 'Add College';

        var url = '/Administration/CreateEdit';
        if (id) {
            url = '/Administration/CreateEdit/?id=' + id;
            title = titleCaption
        }
        $('#myModal').modal('show');
        $('#myModal .modal-title').text(title);
        $.get(url, function (result) {
            $('#myModal .modal-body').html(result);
        }).done(function () {
            utilities.JQValidateParse('#createEdit-sms-form');
        })
    },
    saveCollege: function () {

        var form = $('#createEdit-sms-form');

        form.validate();
        if (form.valid()) {
            utilities.form.save({
                form: form,
                success: function (data, textStatus, xhr) {
                    switch (data.NotifyType) {
                        case 0: $(".response-div").html(data.Html);
                            break;
                    }

                    if (data.Success) {
                        setTimeout(function () {
                            $(".response-div").html('');
                            $('#myModal').modal('hide');
                            USERData.getList();
                        }, 2000)
                    }
                },
                error: function () {
                    form.find(".response-div").html(utilities.error());
                }
            })

        }
    }
}