﻿"use strict";

$(document).ready(function () {
    GSMData.getList();

    $(document).on('click', '.create-sms', function (e) {
        e.preventDefault();
        GSMData.addEdit();
    })

    $(document).on('click', '.edit-sms', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var title = $(this).data('title');
        GSMData.addEdit(id, title);
    })

    $(document).on('click', '.btn-submit-form', function (e) {
        e.preventDefault();

        GSMData.saveCollege();
    })

})

var GSMData = {
    getList: function () {


        var url = '/Administration/GetSMS';
        $('.data-container').html('Loading data...');

        $.get(url, function (result) {
            $('.data-container').html(result);
        }).done(function () {
            GSMData.initTable();
        })
    },
    initTable: function () {
        if ($('#GsmTable').length > 0) {
            $("#GsmTable").dataTable({
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [-1] }
                ]
            });
        }
    },

    addEdit: function (id, titleCaption) {

        var title = 'Add College';

        var url = '/Administration/CreateEdit';
        if (id) {
            url = '/Administration/CreateEdit/?id=' + id;
            title = titleCaption
        }
        $('#myModal').modal('show');
        $('#myModal .modal-title').text(title);
        $.get(url, function (result) {
            $('#myModal .modal-body').html(result);
        }).done(function () {
            utilities.JQValidateParse('#createEdit-sms-form');
        })
    },
    saveCollege: function () {

        var form = $('#createEdit-sms-form');

        form.validate();
        if (form.valid()) {
            utilities.form.save({
                form: form,
                success: function (data, textStatus, xhr) {
                    switch (data.NotifyType) {
                        case 0: $(".response-div").html(data.Html);
                            break;
                    }

                    if (data.Success) {
                        setTimeout(function () {
                            $(".response-div").html('');
                            $('#myModal').modal('hide');
                            GSMData.getList();
                        }, 2000)
                    }
                },
                error: function () {
                    form.find(".response-div").html(utilities.error());
                }
            })

        }
    }
}