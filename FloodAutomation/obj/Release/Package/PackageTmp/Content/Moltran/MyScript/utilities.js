﻿
//Dependencies
//Jquery 1.10, jquery validator, font awesome 4.3.0
var utilities = new function () {
    return {
        init: function () {
            this.self = utilities;
        },

        form: {
            //param consist of form selector and ajax events
            save: function (obj) {
                //return if no parameter
                if (typeof obj === "undefined") {
                    return null;
                }
                //return if no form selector provided
                if (!obj.hasOwnProperty("form")) {
                    return null;
                }

                //Add anti forgery token in headers
                var form = obj.form;
                var token = $(form).find("input[name='__RequestVerificationToken']").val();
                var headers = {};
                headers["__RequestVerificationToken"] = token;

                //Set text of submit button on sending
                var submitBtn = $(form).find(":submit");
                var onSendingText = "Saving...";
                var currentText = "Save";

                if (submitBtn) {
                    if(submitBtn.length === 1)
                        currentText = submitBtn.text();
                    if (submitBtn.data("onsubmittext") && submitBtn.data("onsubmittext").length !== 0) {
                        onSendingText = submitBtn.data("onsubmittext");
                    }
                }

              return $.ajax({
                    url: $(form).attr("action"),
                    data: $(form).serialize(),
                    type: "POST",
                    headers: headers,
                    beforeSend: function (xhr) {
                        if (submitBtn) {
                            submitBtn.prop("disabled", true);
                            submitBtn.text(onSendingText);
                        }
                        if (obj.hasOwnProperty("beforeSend")) {
                            if (jQuery.isFunction(obj.beforeSend)) {
                                obj.beforeSend(xhr);
                            }
                        }
                    },
                    success: function (data, textStatus, xhr) {
                        if (obj.hasOwnProperty("success")) {
                            if (jQuery.isFunction(obj.success)) {
                                obj.success(data, textStatus, xhr);
                            }
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        console.log(textStatus + ": " + errorThrown);
                        if (obj.hasOwnProperty("error")) {
                            if (jQuery.isFunction(obj.error)) {
                                obj.error(xhr, textStatus, errorThrown);
                            }
                        }
                    },
                    complete: function (xhr, textStatus) {
                        if (submitBtn) {
                            submitBtn.prop("disabled", false);
                            submitBtn.text(currentText);
                        }

                        if (obj.hasOwnProperty("complete")) {
                            if (jQuery.isFunction(obj.complete)) {
                                obj.complete(data, textStatus, xhr);
                            }
                        }
                    },
                    statusCode: {
                        404: function () {
                            console.log("404! Page not found.");
                        },
                        501: function () {
                            console.log("501! Internal Server Error.");
                        }
                    }
                });
            },
            login: function(obj) {
                //return if no parameter
                if (typeof obj === "undefined") {
                    return null;
                }
                //return if no form selector provided
                if (!obj.hasOwnProperty("form")) {
                    return null;
                }

                //Add anti forgery token in headers
                var form = obj.form;
                var token = $(form).find("input[name='__RequestVerificationToken']").val();
                var headers = {};
                headers["__RequestVerificationToken"] = token;

                //Set text of submit button on sending
                var submitBtn = $(form).find(":submit");
                var onSendingText = "Logging in...";
                var currentText = "Log in";

                if (submitBtn) {
                    if (submitBtn.length === 1)
                        currentText = submitBtn.text();
                    if (submitBtn.data("onsubmittext") && submitBtn.data("onsubmittext").length !== 0) {
                        onSendingText = submitBtn.data("onsubmittext");
                    }
                }

                return $.ajax({
                    url: $(form).attr("action"),
                    data: $(form).serialize(),
                    type: "POST",
                    headers: headers,
                    beforeSend: function (xhr) {
                        if (submitBtn) {
                            submitBtn.prop("disabled", true);
                            submitBtn.text(onSendingText);
                        }
                        if (obj.hasOwnProperty("beforeSend")) {
                            if (jQuery.isFunction(obj.beforeSend)) {
                                obj.beforeSend(xhr);
                            }
                        }
                    },
                    success: function (data, textStatus, xhr) {
                        if (obj.hasOwnProperty("success")) {
                            if (jQuery.isFunction(obj.success)) {
                                obj.success(data, textStatus, xhr);
                            }
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        console.log(textStatus + ": " + errorThrown);
                        if (obj.hasOwnProperty("error")) {
                            if (jQuery.isFunction(obj.error)) {
                                obj.error(xhr, textStatus, errorThrown);
                            }
                        }
                    },
                    complete: function (xhr, textStatus) {
                        //if (submitBtn) {
                            //submitBtn.prop("disabled", false);
                            //submitBtn.text(currentText);
                        //}

                        if (obj.hasOwnProperty("complete")) {
                            if (jQuery.isFunction(obj.complete)) {
                                obj.complete(data, textStatus, xhr);
                            }
                        }
                    },
                    statusCode: {
                        404: function () {
                            console.log("404! Page not found.");
                        },
                        501: function () {
                            console.log("501! Internal Server Error.");
                        }
                    }
                });
            }
        },

        //Add forms that is dynamically injected in DOM
        //Call this method in success call back of the ajax request
        JQValidateParse: function (form) {
            $(form).removeData("validator");
            $(form).removeData("unobtrusiveValidation");
            $.validator.unobtrusive.parse($(form));
        }
    }
};

var submitForm =
{
    saveForm: function (formSelector) {
    var form = $(formSelector);
    form.find(".response-div").html("");
    form.validate();
    if (form.valid()) {
        return utilities.form.save({
            form: formSelector,
            success: function (data, textStatus, xhr) {
                if (xhr.status === 200) {
                    form.find(".response-div").html(data.Html);

                    return;
                }
                form.find(".response-div").html(utilities.error());
            },
            error: function () {
                form.find(".response-div").html(utilities.error());
            }
        });
    }
}
}
