﻿using FloodAutomation.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FloodAutomation.Models
{
    public class AdminStudentViewModel
    {
        public string UserName { get; set; }
        public string RoleName { get; set; }
        public Gender Gender { get; set; }
        public string UserId { get; set; }
        public string RoleId { get; set; }
        public DateTime Registered { get; set; }
        public string StudentId { get; set; }
        public string PhoneNumber { get; set; }
        public string ImagePath { get; set; }
        public string Email { get; set; }
        public UserStatuses Status { get; set; }
        public string NameIdentifier { get; set; }
    }
    public class AdminEditViewModel
    {
        [Key]
        public string id { get; set; }
        public string PhoneNumber { get; set; }

        public string StudentId { get; set; }

        public string UserName { get; set; }
        public string RoleName { get; set; }



        [Required(ErrorMessage = "Email Address is required")]
        [StringLength(35, ErrorMessage = "Your Email is too long.")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Email is is not valid.")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }

    public class AdminRoleViewModel
    {
        public string Role { get; set; }
        public string RoleId { get; set; }
        public string RoleValue { get; set; }
    }

}