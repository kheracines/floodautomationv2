﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FloodAutomation.Models.ViewModel
{
    public class GSMDataViewModel
    {
        public Guid Id { get; set; }
        [Display(Name = "Contact Number")]
        public string CNumber { get; set; }
        public string Message { get; set; }
        public DateTime DateReceived { get; set; }
    }
}