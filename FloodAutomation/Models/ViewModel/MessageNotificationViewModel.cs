﻿using FloodAutomation.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloodAutomation.Models.ViewModel
{
    public class MessageNotificationViewModel
    {
        public int Id { get; set; }
        public MessageNotify MessageStatus { get; set; }



        [Display(Name = "DateTime")]
        public string ConNotifyId { get; set; }

        [Display(Name = "Area Name")]
        public string DeviceName { get; set; }
        [Display(Name = "Category Name")]
        public string CategoryName { get; set; }

        [Display(Name = "Sender")]
        public string SenderNumber { get; set; }

        [Display(Name = "Status")]
        public string StatusLevel { get; set; }

        [Display(Name = "Data")]
        public string Message { get; set; }

        [Display(Name = "DateTime")]
        public DateTime DateReceived { get; set; }

    }
}