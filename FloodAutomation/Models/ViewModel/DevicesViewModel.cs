﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloodAutomation.Models.ViewModel
{
    public class DevicesViewModel
    {
        public Guid Id { get; set; }
        [Required]
        [Display(Name = "Area")]
        public string NameOfPlace { get; set; }
        [Required]
        [Display(Name = "Longitude")]
        public string Longitude { get; set; }
        [Required]
        [Display(Name = "Latitude")]
        public string Latitude { get; set; }
        [Required]
        [StringLength(10, ErrorMessage = "The Phone Number must be at least 10 characters long. Start with 9", MinimumLength = 10)]
        [RegularExpression(@"[9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]", ErrorMessage = "Phone Number is not valid")]
        public string PhoneNumber { get; set; }
        [Required]
        [Display(Name = "Low / Light")]
        public decimal Low { get; set; }
        [Required]
        [Display(Name = "Moderate")]
        public decimal Moderate { get; set; }
        [Required]
        [Display(Name = "High / Heavy")]
        public decimal High { get; set; }
        [Required]
        [Display(Name = "Very High / Intense")]
        public decimal Very_High { get; set; }

        [Required]
        [Display(Name = "Category")]
        public int? CategoryId { get; set; }
        public SelectList Categorys { get; set; }

        public CategoryViewModel Category { get; set; }
    }
}