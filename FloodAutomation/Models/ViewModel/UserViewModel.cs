﻿using FloodAutomation.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloodAutomation.Models.ViewModel
{
    public class UserViewModel
    {
        public string Id { get; set; }
        [Required]
        [Display(Name = "Role")]
        public string RoleId { get; set; }

        [Display(Name = "Registered")]
        public DateTime DateRegistered { get; set; }
        [Display(Name = "Modified")]
        public DateTime DateModified { get; set; }
        [Required]
        public string Email { get; set; }

        public string UserName { get; set; }
        [Required]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        [Required]

        public string NameIdentifier { get; set; }
        [Required]

        public Gender Gender { get; set; }
        [Required]

        public UserStatuses Status { get; set; }
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [Display(Name = "Temp Password")]
        [MinLength(6)]
        public string TempPassword { get; set; }
        public SelectList Roles { get; set; }
        public string CurrentRole { get; set; }

    

    }

    public class UserDetailsViewModel
    {
        public string Id { get; set; }
        public string NameIdentifier { get; set; }
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public UserStatuses Status { get; set; }
        public string CurrentAction { get; set; }
        public string CurrentRole { get; set; }
    }
}