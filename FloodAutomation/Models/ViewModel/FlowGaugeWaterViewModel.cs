﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloodAutomation.Models.ViewModel
{
     class FlowGaugeWaterViewModel
    {
    }

    public class FlowMeterViewModel
    {
        public int Id { get; set; }

        public string ConNotifyId { get; set; }

        [Required]
        [Display(Name = "Sender")]
        public string SenderNumber { get; set; }
        [Required]
        [Display(Name = "Message")]
        public string Message { get; set; }
        [Required]
        [Display(Name = "Received")]
        public DateTime DateReceived { get; set; }
        [Required]
        [Display(Name = "Status")]
        public string Status { get; set; }
        [Required]
        [Display(Name = "Index")]
        public string Zindex { get; set; }
        [Required]
        [Display(Name = "Device")]
        public Guid? DeviceId { get; set; }
        public SelectList Device { get; set; }

        public DevicesViewModel Devices { get; set; }
    }
    public class RainGaugeViewModel
    {
        public int Id { get; set; }

        public string ConNotifyId { get; set; }

        [Required]
        [Display(Name = "Sender")]
        public string SenderNumber { get; set; }
        [Required]
        [Display(Name = "Message")]
        public string Message { get; set; }
        [Required]
        [Display(Name = "Received")]
        public DateTime DateReceived { get; set; }
        [Required]
        [Display(Name = "Status")]
        public string Status { get; set; }
        [Required]
        [Display(Name = "Index")]
        public string Zindex { get; set; }
        [Required]
        [Display(Name = "Device")]
        public Guid? DeviceId { get; set; }
        public SelectList Device { get; set; }

        public DevicesViewModel Devices { get; set; }
    }
    public class WaterLevelViewModel
    {
        public int Id { get; set; }

        public string ConNotifyId { get; set; }

        [Required]
        [Display(Name = "Sender")]
        public string SenderNumber { get; set; }
        [Required]
        [Display(Name = "Message")]
        public string Message { get; set; }
        [Required]
        [Display(Name = "Received")]
        public DateTime DateReceived { get; set; }
        [Required]
        [Display(Name = "Status")]
        public string Status { get; set; }
        [Required]
        [Display(Name = "Index")]
        public string Zindex { get; set; }
        [Required]
        [Display(Name = "Device")]
        public Guid? DeviceId { get; set; }
        public SelectList Device { get; set; }

        public DevicesViewModel Devices { get; set; }
    }
}