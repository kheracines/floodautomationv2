﻿using FloodAutomation.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FloodAutomation.Models.ViewModel
{
    public class UserNotificationViewModel
    {
        public int Id { get; set; }
        public string userEmail { get; set; }
        public DateTime DateTime { get; set; }
        public SessionStatus SessionStatus { get; set; }
        public UserNotify NotifyUserStatus { get; set; }
    }
}