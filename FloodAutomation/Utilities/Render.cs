﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloodAutomation.Utilities
{
    public static class Renders
    {
        public static string ReturnEmptyStringIfStringIsNullOrEmpty(this string str, bool isAdditionalStrOnLast = true, string additionalStr = "")
        {
            additionalStr = !string.IsNullOrEmpty(additionalStr) ? additionalStr : string.Empty;

            string resultStr = isAdditionalStrOnLast ? string.Format("{0}{1}", str, additionalStr)
                    : !isAdditionalStrOnLast ? string.Format("{0}{1}", additionalStr, str)
                    : string.Format("{0}{1}", str, additionalStr);

            return !string.IsNullOrEmpty(str) && !string.IsNullOrEmpty(str.Trim()) ? resultStr : string.Empty;
        }
        public static string RenderPartialView(this Controller controller, string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = controller.ControllerContext.RouteData.GetRequiredString("action");

            controller.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
    }
}