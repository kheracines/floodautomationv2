﻿using AutoMapper;
using FloodAutomation.Models.ViewModel;
using FloodAutomation.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FloodAutomation.App_Start
{
    public static class AutoMapperConfig
    {
        public static MapperConfiguration MapperConfiguration;

        public static void RegisterMappings()
        {
            MapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<GSMData, GSMDataViewModel>().ReverseMap();
                cfg.CreateMap<User, UserViewModel>().ReverseMap();
                cfg.CreateMap<Role, RoleViewModel>().ReverseMap();
                cfg.CreateMap<Category, CategoryViewModel>().ReverseMap();
                cfg.CreateMap<Devices, DevicesViewModel>().ReverseMap();
                cfg.CreateMap<FlowMeter, FlowMeterViewModel>().ReverseMap();
                cfg.CreateMap<RainGauge, RainGaugeViewModel>().ReverseMap();
                cfg.CreateMap<WaterLevel, WaterLevelViewModel>().ReverseMap();
                cfg.CreateMap<MessageNotification, MessageNotificationViewModel>().ReverseMap();
                cfg.CreateMap<UserNotification, UserNotificationViewModel>().ReverseMap();

            });
        }
    }
}