﻿using FloodAutomation.Models.ViewModel;
using FloodAutomation.Core.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FloodAutomation.Core.Domain;
using Newtonsoft.Json;

namespace FloodAutomation.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IUnitofWork _context;

        public HomeController(IUnitofWork context)
        {
            _context = context;

        }
        public ActionResult Index()
        {
            return View();
        }


        public ContentResult GetDevicesBack()
        {
            var Devices = _context.DevicesRepo.Get(orderBy: a => a.OrderBy(n => n.NameOfPlace), includeProperties: o => o.Category).Select(a => MapperInstance.Map<Devices, DevicesViewModel>(a));

            List<Dictionary<string, object>> dataResult = new List<Dictionary<string, object>>();
            foreach (var item in Devices)
            {

                Dictionary<string, object> aSeries = new Dictionary<string, object>();

                aSeries["Id"] = item.Id;
                aSeries["PlaceName"] = item.NameOfPlace;
                aSeries["GeoLong"] = item.Longitude;
                aSeries["GeoLat"] = item.Latitude;
                aSeries["Category"] = item.CategoryId;

                dataResult.Add(aSeries);


            }

            return Content(JsonConvert.SerializeObject(dataResult), "application/json");
        }
        

        public ActionResult WaterGetGMAPDevices(Guid? Id)
        {
            var WaterLevel = _context.WaterLevelRepo.Get(orderBy: a => a.OrderBy(n => n.DateReceived), includeProperties: o => o.Devices).Select(a => MapperInstance.Map<WaterLevel, WaterLevelViewModel>(a));
            var Devices = _context.DevicesRepo.Get(q => q.Id == Id, orderBy: a => a.OrderBy(n => n.NameOfPlace), includeProperties: o => o.Category).Select(a => MapperInstance.Map<Devices, DevicesViewModel>(a));

            List<Dictionary<string, object>> dataResult = new List<Dictionary<string, object>>();
            foreach (var item in Devices)
            {

                Dictionary<string, object> aSeries = new Dictionary<string, object>();
                if (item.CategoryId == 3)
                {

                    aSeries["name"] = item.NameOfPlace;
                    aSeries["data"] = new List<object[]>();

                    foreach (var itemwater in WaterLevel)
                    {
                        if (itemwater.DeviceId == item.Id)
                        {
                            DateTime now = DateTime.Now;
                            DateTime yesterday = now.AddDays(-1);
                            if (itemwater.DateReceived >= yesterday)
                            {

                                object[] values = new object[2];
                                values[0] = itemwater.DateReceived;
                                values[1] = Convert.ToDecimal(itemwater.Message);
                                ((List<object[]>)aSeries["data"]).Add((values));
                            }
                        }
                    }
                    dataResult.Add(aSeries);
                }

            }

            return Content(JsonConvert.SerializeObject(dataResult), "application/json");
        }


        public ActionResult FlowGetGMAPDevices(Guid? Id)
        {
            var FlowMeter = _context.FlowMeterRepo.Get(orderBy: a => a.OrderBy(n => n.DateReceived), includeProperties: o => o.Devices).Select(a => MapperInstance.Map<FlowMeter, FlowMeterViewModel>(a));
            var Devices = _context.DevicesRepo.Get(q => q.Id == Id, orderBy: a => a.OrderBy(n => n.NameOfPlace), includeProperties: o => o.Category).Select(a => MapperInstance.Map<Devices, DevicesViewModel>(a));

            List<Dictionary<string, object>> dataResult = new List<Dictionary<string, object>>();
            foreach (var item in Devices)
            {

                Dictionary<string, object> aSeries = new Dictionary<string, object>();
                if (item.CategoryId == 2)
                {

                    aSeries["name"] = item.NameOfPlace;
                    aSeries["data"] = new List<object[]>();

                    foreach (var itemFlow in FlowMeter)
                    {
                        if (itemFlow.DeviceId == item.Id)
                        {
                            DateTime now = DateTime.Now;
                            DateTime yesterday = now.AddDays(-1);
                            if (itemFlow.DateReceived >= yesterday)
                            {


                                object[] values = new object[2];
                                values[0] = itemFlow.DateReceived;
                                values[1] = Convert.ToDecimal(itemFlow.Message);
                                ((List<object[]>)aSeries["data"]).Add((values));
                            }
                        }
                    }
                    dataResult.Add(aSeries);
                }

            }

            return Content(JsonConvert.SerializeObject(dataResult), "application/json");
        }

        public ActionResult RainGetGMAPDevices(Guid? Id)
        {
            var RaingGauge = _context.RainGaugeRepo.Get(orderBy: a => a.OrderBy(n => n.DateReceived), includeProperties: o => o.Devices).Select(a => MapperInstance.Map<RainGauge, RainGaugeViewModel>(a));
            var Devices = _context.DevicesRepo.Get(q => q.Id == Id, orderBy: a => a.OrderBy(n => n.NameOfPlace), includeProperties: o => o.Category).Select(a => MapperInstance.Map<Devices, DevicesViewModel>(a));

            List<Dictionary<string, object>> dataResult = new List<Dictionary<string, object>>();
            foreach (var item in Devices)
            {

                Dictionary<string, object> aSeries = new Dictionary<string, object>();
                if (item.CategoryId == 1)
                {

                    aSeries["name"] = item.NameOfPlace;
                    aSeries["data"] = new List<object[]>();

                    foreach (var itemRain in RaingGauge)
                    {
                        if (itemRain.DeviceId == item.Id)
                        {
                            DateTime now = DateTime.Now;
                            DateTime yesterday = now.AddDays(-1);
                            if (itemRain.DateReceived >= yesterday )
                            {

                                object[] values = new object[2];
                                values[0] = itemRain.DateReceived;
                                values[1] = Convert.ToDecimal(itemRain.Message);
                                ((List<object[]>)aSeries["data"]).Add((values));
                            }
                        }
                    }
                    dataResult.Add(aSeries);
                }

            }

            return Content(JsonConvert.SerializeObject(dataResult), "application/json");
        }



        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}