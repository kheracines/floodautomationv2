﻿using FloodAutomation.Core.Interfaces.Repository;
using FloodAutomation.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FloodAutomation.Core.Domain;
using FloodAutomation.Hubs;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using FloodAutomation.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.IO.Ports;
using System.Threading;
using System.Text;
using FloodAutomation.Models.GSM;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using FloodAutomation.Core.Enums;
using System.IO;

namespace FloodAutomation.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdministrationController : BaseController
    {
        private readonly IUnitofWork _context;
        private AutoResetEvent receiveNow;
        private SerialPort port;

        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public AdministrationController(IUnitofWork context)
        {
            _context = context;
            //_contextUser = new ApplicationDbContext();
            //UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_contextUser));
            port = null;
            receiveNow = new AutoResetEvent(false);
            //read();

        }


        //public UserManager<ApplicationUser> UserManager { get; set; }
        //public ApplicationDbContext _contextUser { get; set; }
        //public static List<AdminStudentViewModel> userList = new List<AdminStudentViewModel>();
        //public static List<SelectListItem> roleList = new List<SelectListItem>();


        #region Dashboard
        public ActionResult Index()
        {

            return View();
        }
        public ActionResult CounterDevices()
        {
            int CounterDevices = 1;
            var Devices = _context.DevicesRepo.Get(orderBy: a => a.OrderBy(n => n.NameOfPlace), includeProperties: o => o.Category).Select(a => MapperInstance.Map<Devices, DevicesViewModel>(a));
            foreach (var item in Devices)
            {
                ViewBag.CounterDevices = CounterDevices++;
            }
            return PartialView();
        }
        public ActionResult CounterRegUsers()
        {
            int CounterRegUsers = 1;
            var users = _context.UserRepo.Get(orderBy: c => c.OrderBy(o => o.NameIdentifier)).Select(c => MapperInstance.Map<User, UserViewModel>(c)).ToList();
            foreach (var item in users)
            {
                ViewBag.CounterRegUsers = CounterRegUsers++;
            }
            return PartialView();
        }
        public ActionResult UserCounter()
        {
            return PartialView();
        }
        public ActionResult MessageCounter()
        {
            int MessageCounter = 1;
            var notify = _context.MessageNotificationRepo.Get(orderBy: a => a.OrderByDescending(n => n.Id)).Select(a => MapperInstance.Map<MessageNotification, MessageNotificationViewModel>(a));
            foreach (var item in notify)
            {
                ViewBag.MessageCounter = MessageCounter++;

            }
            return PartialView();
        }
        #endregion


        #region TestSMS
        public ActionResult SMS()
        {
            return View();
        }
        public ActionResult GetSMS()
        {
            var SMS = _context.GSMDataRepo.Get(orderBy: a => a.OrderBy(n => n.CNumber)).Select(a => MapperInstance.Map<GSMData, GSMDataViewModel>(a));
            return PartialView("_GetSMS", SMS);
        }
        public ActionResult CreateEdit(Guid? Id)
        {
            var model = new GSMDataViewModel();
            if (Id.HasValue)
            {
                model = MapperInstance.Map<GSMData, GSMDataViewModel>(_context.GSMDataRepo.GetById(Id));
            }
            return PartialView("_CreateEdit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateEdit(GSMDataViewModel model)
        {
            return SaveBasicForm(() =>
            {
                if (model.Id != Guid.Empty)
                {
                    var dev = _context.GSMDataRepo.GetById(model.Id);
                    dev.CNumber = model.CNumber;
                    //dev.DateReceived = model.DateReceived;
                    dev.Message = model.Message;

                    _context.GSMDataRepo.Update(dev);
                    _context.Complete();
                    //  SMSHub.NotifyDataInformationToAllClients();
                }
                else
                {
                    if (model.Id == Guid.Empty)
                    {
                        model.Id = Guid.NewGuid();
                        model.DateReceived = DateTime.Now;
                        var newDev = MapperInstance.Map<GSMData>(model);
                        _context.GSMDataRepo.Add(newDev);
                        _context.Complete();
                        // SMSHub.NotifyDataInformationToAllClients();
                    }
                }
            });
        }
        #endregion


        #region User
        public ActionResult Users()
        {
            return View();
        }
        public async Task<ActionResult> GetUserList()
        {
            var users = _context.UserRepo.Get(orderBy: c => c.OrderBy(o => o.NameIdentifier)).Select(c => MapperInstance.Map<User, UserViewModel>(c)).ToList();

            foreach (var usr in users)
            {
                var userRoles = await UserManager.GetRolesAsync(usr.Id);

                if (userRoles != null)
                {
                    usr.CurrentRole = userRoles.FirstOrDefault();
                }
            }

            return PartialView("_GetUsers", users);

        }


        public async Task<ActionResult> CreateEditUsers(string id = "")
        {
            var model = new UserViewModel();

            if (!string.IsNullOrEmpty(id))
            {
                model = MapperInstance.Map<User, UserViewModel>(_context.UserRepo.GetById(id));

                //get the current role of the user
                var userRoles = await UserManager.GetRolesAsync(id);

                if (userRoles != null)
                {
                    var role = _context.RoleRepo.Get(c => c.Name == userRoles.FirstOrDefault()).FirstOrDefault();

                    if (role != null)
                    {
                        model.RoleId = role.Id;
                    }
                }
            }

            model.Roles = new SelectList(_context.RoleRepo.Get(orderBy: c => c.OrderBy(x => x.Name)), "Id", "Name", model.RoleId);


            return PartialView("_CreateEditUsers", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public async Task<ActionResult> CreateEditUsers(UserViewModel model)
        {
            RequestResponseModel response = new RequestResponseModel();
            string userId = "";

            if (!ModelState.IsValid)
            {
                response.Message = GetValidationErrors();
                return ReturnErrorResponse(response);
            }

            try
            {
                if (!string.IsNullOrEmpty(model.Id))
                {
                    userId = model.Id;

                    var userToEdit = _context.UserRepo.GetById(model.Id);

                    //check if email is being changed
                    if (!userToEdit.Email.Equals(model.Email))
                    {
                        if (IsEmailExisting(model.Email))
                        {
                            response.Message = "Email cannot be changed the email is already in used. User's email must be unique.";

                            return ReturnErrorResponse(response);
                        }
                    }
                    userToEdit.NameIdentifier = model.NameIdentifier;
                    userToEdit.Gender = model.Gender;
                    userToEdit.Status = model.Status;
                    userToEdit.Email = model.Email;
                    userToEdit.PhoneNumber = model.PhoneNumber;
                    userToEdit.DateModified = DateTime.Now;


                    _context.UserRepo.Update(userToEdit);
                    _context.Complete();

                    //check if user role is being changed
                    var role = _context.RoleRepo.GetById(model.RoleId);

                    var isUserInRole = await UserManager.IsInRoleAsync(model.Id, role.Name);

                    //user not belong to role then its being changed, we need to update the role
                    if (!isUserInRole)
                    {
                        //remove the previous role of the user

                        var currentRole = await UserManager.GetRolesAsync(model.Id);
                        if (currentRole != null)
                        {
                            await UserManager.RemoveFromRolesAsync(model.Id, currentRole.ToArray());
                        }

                        //add the user to the new role
                        await UserManager.AddToRoleAsync(model.Id, role.Name);
                    }

                    //check if password has changed
                    if (!string.IsNullOrEmpty(model.TempPassword))
                    {
                        //remove and apply the new password
                        await UserManager.RemovePasswordAsync(model.Id);

                        await UserManager.AddPasswordAsync(model.Id, model.TempPassword);
                    }

                }
                else
                {
                    if (IsEmailExisting(model.Email))
                    {
                        response.Message = "Email is already in used. User's email must be unique.";
                        return ReturnErrorResponse(response);
                    }

                    string tempPassowrd = !string.IsNullOrEmpty(model.TempPassword) ? model.TempPassword : RandomCharGenerator.RandomString(6, true);

                    var newUser = new ApplicationUser { DateModified = DateTime.Now, DateRegistered = DateTime.Now, UserName = model.Email, Email = model.Email, NameIdentifier = model.NameIdentifier, PhoneNumber = model.PhoneNumber, Gender = model.Gender, Status = model.Status };

                    var result = await UserManager.CreateAsync(newUser, tempPassowrd);

                    if (result.Succeeded)
                    {
                        userId = newUser.Id;

                        var roleSelected = _context.RoleRepo.GetById(model.RoleId);

                        await UserManager.AddToRoleAsync(newUser.Id, roleSelected.Name);
                    }

                }

                response.Message = GetMessage(ResponseMessageType.Save);
                return ReturnSuccessResponse(response, userId);
            }
            catch (Exception)
            {
                response.Message = GetMessage(ResponseMessageType.Error);
            }

            return ReturnErrorResponse(response);
        }
        private bool IsEmailExisting(string email)
        {
            var user = _context.UserRepo.Get(c => c.Email == email).FirstOrDefault();
            if (user == null)
            {
                return false;
            }

            return true;
        }


        //public async Task<ActionResult> _GetUsers(AdminStudentViewModel model)
        //{
        //    userList.Clear();
        //    IList<ApplicationUser> users = _contextUser.Users.ToList();

        //    foreach (var user in users)
        //    {
        //        var roles = await UserManager.GetRolesAsync(user.Id);
        //        foreach (var role in roles)
        //        {
        //            model.RoleName = role;

        //        }

        //        model.UserName = user.UserName;
        //        model.UserId = user.Id;
        //        model.Email = user.Email;
        //        model.PhoneNumber = user.PhoneNumber;
        //        model.NameIdentifier = user.NameIdentifier;
        //        model.Status = user.Status;
        //        userList.Add(new AdminStudentViewModel() { Status = model.Status, NameIdentifier = model.NameIdentifier, UserName = model.UserName, RoleName = model.RoleName, UserId = model.UserId, RoleId = model.RoleId, StudentId = model.StudentId, PhoneNumber = model.PhoneNumber, Email = model.Email, Registered = model.Registered });
        //        model.RoleName = null;

        //    }
        //    return PartialView("_GetUsers");
        //}
        #endregion


        #region Devices
        public ActionResult Devices()
        {
            return View();
        }

        public ActionResult _GetDevices()
        {
            var Devices = _context.DevicesRepo.Get(orderBy: a => a.OrderBy(n => n.NameOfPlace), includeProperties: o => o.Category).Select(a => MapperInstance.Map<Devices, DevicesViewModel>(a));

            return PartialView("_GetDevices", Devices);
        }

        [HttpGet]
        public ActionResult CreateEditDevices(Guid? Id)
        {
            var model = new DevicesViewModel();
            if (Id.HasValue)
            {
                model = MapperInstance.Map<Devices, DevicesViewModel>(_context.DevicesRepo.GetById(Id));

            }
            model.Categorys = new SelectList(_context.CategoryRepo.Get(orderBy: c => c.OrderBy(x => x.CategoryName)), "Id", "CategoryName", model.CategoryId);

            return PartialView("_CreateEditDevices", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateEditDevices(DevicesViewModel model)
        {
            if (model.Low < 0 || model.Low >= model.Moderate)
            {
                ModelState.AddModelError("Error", "Low/light is less than 0 or greater than moderate");
            }
            else if (model.Moderate <= model.Low || model.Moderate >= model.High)
            {
                ModelState.AddModelError("Error", "Moderate is less than High/Heavy or greater than Low");
            }
            else if (model.High <= model.Moderate || model.High >= model.Very_High)
            {
                ModelState.AddModelError("Error", "High/Heavy is less than Very High/Intense or greater than Moderate");
            }
            else if (model.Very_High < model.High)
            {
                ModelState.AddModelError("Error", "Very High/Intense is Greater than High/Heavy");
            }

            return SaveBasicForm(() =>
            {
                if (model.Id != Guid.Empty)
                {

                    var dev = _context.DevicesRepo.GetById(model.Id);
                    dev.NameOfPlace = model.NameOfPlace;
                    dev.Longitude = model.Longitude;
                    dev.Latitude = model.Latitude;
                    dev.PhoneNumber = model.PhoneNumber;
                    dev.CategoryId = model.CategoryId;
                    dev.Low = model.Low;
                    dev.Moderate = model.Moderate;
                    dev.High = model.High;
                    dev.Very_High = model.Very_High;

                    _context.DevicesRepo.Update(dev);
                    _context.Complete();
                    //  SMSHub.NotifyDataInformationToAllClients();
                }
                else
                {
                    if (model.Id == Guid.Empty)
                    {

                        model.Id = Guid.NewGuid();
                        var newDev = MapperInstance.Map<Devices>(model);
                        _context.DevicesRepo.Add(newDev);
                        _context.Complete();
                        // SMSHub.NotifyDataInformationToAllClients();
                    }
                }
            });
        }
        [HttpPost]
        public ActionResult DeleteDevice(Guid? Id)
        {
            return SaveBasicForm(() =>
            {
                if (Id != Guid.Empty)
                {
                    var sc = _context.DevicesRepo.GetById(Id);
                    _context.DevicesRepo.Remove(sc);
                    _context.Complete();
                }
            });
        }

        #endregion


        #region GETFloodMonitoring
        public ActionResult RainGauge()
        {
            return View();
        }

        public ActionResult _GetRainGauge()
        {
            var RainGauge = _context.RainGaugeRepo.Get(orderBy: a => a.OrderByDescending(n => n.DateReceived), includeProperties: o => o.Devices).Select(a => MapperInstance.Map<RainGauge, RainGaugeViewModel>(a));

            return PartialView("_GetRainGauge", RainGauge);
        }

        public ActionResult WaterLevel()
        {
            return View();
        }
        public ActionResult _GetWaterLevel()
        {
            var WaterLevel = _context.WaterLevelRepo.Get(orderBy: a => a.OrderByDescending(n => n.DateReceived), includeProperties: o => o.Devices).Select(a => MapperInstance.Map<WaterLevel, WaterLevelViewModel>(a));

            return PartialView("_GetWaterLevel", WaterLevel);
        }
        public ActionResult FlowMeter()
        {
            return View();
        }
        public ActionResult _GetFlowMeter()
        {
            var FlowMeter = _context.FlowMeterRepo.Get(orderBy: a => a.OrderByDescending(n => n.DateReceived), includeProperties: o => o.Devices).Select(a => MapperInstance.Map<FlowMeter, FlowMeterViewModel>(a));
            return PartialView("_GetFlowMeter", FlowMeter);
        }
        #endregion


        #region GSM
        //string inputs = ExecCommandread("AT+COPS?", 300, "Test");
        //string inputss = ExecCommandread("AT+CSQ", 300, "Test");//get the signal Quality
        //                                                        //string inputsss = ExecCommandread("AT+CSQ<CR>", 300, "Test");//get the signal Quality

        //MessageBox.Show(inputss);
        //        // Use character set "ISO 8859-1"
        //        ExecCommandread("AT+CSCS=\"8859-1\"", 300, "Failed to set character set.");
        //        // Select SIM storage
        //        ExecCommandread("AT+CPMS=\"SM\"", 300, "Failed to select message storage.");
        //// Read the messages
        //string input = ExecCommandread("AT+CMGL=\"ALL\"", 5000, "Failed to read the messages.");
        //messages = ParseMessages(input);
        //Cursor.Current = Cursors.Default;
        //        string test = inputss.Split(':')[1];

        //textBox1.Text = test.Split(',')[0];

        private string portName = "COM6";
        public ActionResult GetReadSMS()
        {

            read();
            return PartialView("_GetReadSMS");
        }


        public void read()
        {
            
            // Get and verify port name
            //  string portName = cboPort.Text;


            // Set up the phone and read the messages
            ShortMessageCollection messages = null;
            string todelete = "";
            string input = "";
            try
            {
                this.port = OpenPort(portName);
                // Check connection
                ExecCommandread("AT", 300, "No phone connected at " + portName + ".");
                // Use message format "Text mode"
                ExecCommandread("AT+CMGF=1", 300, "Failed to set message format.");
                // Use character set "ISO 8859-1"
                //ExecCommandread("AT+CSCS=\"8859-1\"", 300, "Failed to set character set.");
                // Select SIM storage
                ExecCommandread("AT+CPMS=\"SM\"", 300, "Failed to select message storage.");
                // Read the messages
                input = ExecCommandread("AT+CMGL=\"ALL\"", 5000, "Failed to read the messages.");
                messages = ParseMessages(input);


                var sd = messages.ToList();
                var device = _context.DevicesRepo.Get(orderBy: a => a.OrderByDescending(n => n.NameOfPlace), includeProperties: o => o.Category).Select(a => MapperInstance.Map<Devices, DevicesViewModel>(a));
                var WaterLevel = _context.WaterLevelRepo.Get(orderBy: a => a.OrderByDescending(n => n.DateReceived), includeProperties: o => o.Devices).Select(a => MapperInstance.Map<WaterLevel, WaterLevelViewModel>(a));

                if (messages != null)
                {
                    //lvwMessages.BeginUpdate();
                    foreach (ShortMessage msg in messages)
                    {


                        foreach (var rn in device)
                        {
                            if (msg.Sender.Contains(rn.PhoneNumber))
                            {
                                if (rn.Category.Id == 1)
                                {


                                    RainGauge modelr = new RainGauge();
                                    modelr.ConNotifyId = Convert.ToString(DateTime.Now);
                                    modelr.DeviceId = rn.Id;
                                    modelr.SenderNumber = msg.Sender;

                                    var getmsg = msg.Message;
                                    getmsg = Regex.Replace(getmsg, @"[^0-9\.]+", string.Empty);

                                    modelr.Message = getmsg;
                                    var dateSent = msg.Sent.Split(',')[0];
                                    var timeSent = (msg.Sent.Split(',')[1]).Split('+')[0];
                                    string GetDateTime = string.Format("{0} {1}", dateSent, timeSent);
                                    string SaveDatetime = "20" + GetDateTime;
                                  
                                    modelr.DateReceived = Convert.ToDateTime(SaveDatetime);
                                    if (rn.Low >= Convert.ToDecimal(getmsg))
                                    {
                                        modelr.Status = "Light";

                                    }
                                    else if (rn.Moderate >= Convert.ToDecimal(getmsg))
                                    {
                                        modelr.Status = "Moderate";
                                    }
                                    else if (rn.High >= Convert.ToDecimal(getmsg))
                                    {
                                        modelr.Status = "Heavy";
                                    }
                                    else if (rn.Very_High >= Convert.ToDecimal(getmsg))
                                    {
                                        modelr.Status = "Intense";
                                    }
                                    else
                                    {
                                        modelr.Status = "Torrential";

                                    }
                                    modelr.Zindex = msg.Index.ToString();

                                    MessageNotification Mmodelr = new MessageNotification();
                                    Mmodelr.MessageStatus = Core.Enums.MessageNotify.UnRead;
                                    Mmodelr.ConNotifyId = modelr.ConNotifyId;
                                    Mmodelr.DateReceived = Convert.ToDateTime(SaveDatetime);
                                    Mmodelr.Message = getmsg;
                                    Mmodelr.SenderNumber = msg.Sender;
                                    Mmodelr.DeviceName = rn.NameOfPlace;
                                    Mmodelr.CategoryName = "Rain Gauge";
                                    Mmodelr.StatusLevel = modelr.Status;
                                    var newMessageNotify = MapperInstance.Map<MessageNotification>(Mmodelr);
                                    _context.MessageNotificationRepo.Add(newMessageNotify);

                                    var newDev = MapperInstance.Map<RainGauge>(modelr);
                                    _context.RainGaugeRepo.Add(newDev);
                                    _context.Complete();
                                    SMSHub.NotifyDataInformationToAllClients();

                                }
                                if (rn.CategoryId == 2)
                                {
                                    FlowMeter models = new FlowMeter();
                                    models.ConNotifyId = Convert.ToString(DateTime.Now);
                                    models.DeviceId = rn.Id;
                                    models.SenderNumber = msg.Sender;

                                    //var ms = msg.Message.Split(':')[2];
                                    //var msf = ms.Split('m')[0];
                                    //var f = msf.Replace(" ", String.Empty);
                                    var getmsg = msg.Message;
                                    getmsg = Regex.Replace(getmsg, @"[^0-9\.]+", string.Empty);
                                    string finalGetMsg = getmsg.Remove(getmsg.Length - 1);
                                    models.Message = finalGetMsg;


                                    var dateSent = msg.Sent.Split(',')[0];
                                    var timeSent = (msg.Sent.Split(',')[1]).Split('+')[0];
                                    string GetDateTime = string.Format("{0} {1}", dateSent, timeSent);
                                    string SaveDatetime = "20" + GetDateTime;
                                    models.DateReceived = Convert.ToDateTime(SaveDatetime);

                                    if (rn.Low >= Convert.ToDecimal(finalGetMsg))
                                    {
                                        models.Status = "Low Flood";

                                    }
                                    else if (rn.Moderate >= Convert.ToDecimal(finalGetMsg))
                                    {
                                        models.Status = "Moderate Flood";
                                    }
                                    else if (rn.High >= Convert.ToDecimal(finalGetMsg))
                                    {
                                        models.Status = "High Flood";
                                    }
                                    else if (rn.Very_High >= Convert.ToDecimal(finalGetMsg))
                                    {
                                        models.Status = "Very High Flood";
                                    }

                                    models.Zindex = msg.Index.ToString();

                                    MessageNotification Mmodels = new MessageNotification();
                                    Mmodels.MessageStatus = Core.Enums.MessageNotify.UnRead;
                                    Mmodels.ConNotifyId = models.ConNotifyId;
                                    Mmodels.DateReceived = models.DateReceived;
                                    Mmodels.Message = finalGetMsg;
                                    Mmodels.SenderNumber = models.SenderNumber;
                                    Mmodels.DeviceName = rn.NameOfPlace;
                                    Mmodels.CategoryName = "Flow Meter";
                                    Mmodels.StatusLevel = models.Status;

                                    var newMessageNotify = MapperInstance.Map<MessageNotification>(Mmodels);
                                    _context.MessageNotificationRepo.Add(newMessageNotify);

                                    var newDev = MapperInstance.Map<FlowMeter>(models);
                                    _context.FlowMeterRepo.Add(newDev);
                                    _context.Complete();
                                    SMSHub.NotifyDataInformationToAllClients();

                                }
                                if (rn.CategoryId == 3)
                                {
                                    WaterLevel model = new WaterLevel();
                                    model.ConNotifyId = Convert.ToString(DateTime.Now);
                                    model.DeviceId = rn.Id;
                                    model.SenderNumber = msg.Sender;

                                    var getmsg = msg.Message;
                                    getmsg = Regex.Replace(getmsg, @"[^0-9\.]+", string.Empty);
                                    model.Message = getmsg;

                                    var dateSent = msg.Sent.Split(',')[0];
                                    var timeSent = (msg.Sent.Split(',')[1]).Split('+')[0];
                                    string GetDateTime = string.Format("{0} {1}", dateSent, timeSent);
                                    string SaveDatetime = "20" + GetDateTime;
                                    model.DateReceived = Convert.ToDateTime(SaveDatetime);

                                 

                                    if (rn.Low >= Convert.ToDecimal(getmsg))
                                    {
                                        model.Status = "Low Flood";

                                    }
                                    else if (rn.Moderate >= Convert.ToDecimal(getmsg))
                                    {
                                        model.Status = "Moderate/Normal Flood";
                                    }
                                    else if (rn.High >= Convert.ToDecimal(getmsg))
                                    {
                                        model.Status = "High Flood";
                                    }
                                    else if (rn.Very_High >= Convert.ToDecimal(getmsg))
                                    {
                                        model.Status = "Very High Flood";
                                    }

                                    model.Zindex = msg.Index.ToString();

                                    MessageNotification Mmodel = new MessageNotification();
                                    Mmodel.MessageStatus = Core.Enums.MessageNotify.UnRead;
                                    Mmodel.ConNotifyId = model.ConNotifyId;
                                    Mmodel.DateReceived = model.DateReceived;
                                    Mmodel.Message = getmsg;
                                    Mmodel.SenderNumber = model.SenderNumber;
                                    Mmodel.DeviceName = rn.NameOfPlace;
                                    Mmodel.CategoryName = "Water Level";
                                    Mmodel.StatusLevel = model.Status;

                                    var newMessageNotify = MapperInstance.Map<MessageNotification>(Mmodel);
                                    _context.MessageNotificationRepo.Add(newMessageNotify);

                                    var newDev = MapperInstance.Map<WaterLevel>(model);
                                    _context.WaterLevelRepo.Add(newDev);
                                    _context.Complete();
                                    SMSHub.NotifyDataInformationToAllClients();


                                }

                            }

                        }
                        todelete = ExecCommandread("AT+CMGD=1,4", 300, "Delete Message");

                        if (input.EndsWith("\r\nOK\r\n"))
                        {
                            ParseMessages(todelete);

                        }
                        if (input.Contains("ERROR"))
                        {

                        }

                    }

                }


                string con = ExecCommandread("AT", 300, "Failed Connection").Split('T')[1];
                string connection = con;
                if (connection.Contains("OK"))
                {
                    ViewBag.GSMStatus = "Open";
                    string signal = ExecCommandread("AT+CSQ", 300, "Faild to set signal");

                    string getsignal = signal.Split(':')[1];
                    int getsignalright = Convert.ToInt32(getsignal.Split(',')[1]);
                    int left = Convert.ToInt32(getsignal.Split(',')[0]);
                    ViewBag.SignalQuality = left + getsignalright;
                }
                //ReadSMS.NotifyDataReadToAllClients();
                //MessageHub.NotifyDataMessageToAllClients();

            }

            catch (Exception ex)
            {
                ErrorLog(ex.Message);
                return;
            }

            finally
            {
                if (port != null)
                {
                    ClosePort(this.port);
                    this.port = null;
                }
            }



        }


        private ShortMessageCollection ParseMessages(string input)
        {
            ShortMessageCollection messages = new ShortMessageCollection();
            //Regex r = new Regex(@"\+CMGL: (\d+),""(.+)"",""(.+)"",(.*),""(.+)""\r\n(.+)\r\n");
            Regex r = new Regex(@"\+CMGL: (\d+),""(.+)"",""(.+)"",(.*),""(.+)""\r\n([^\r]+)\r\n", RegexOptions.Multiline);
            Match m = r.Match(input);
            while (m.Success)
            {
                ShortMessage msg = new ShortMessage();
                msg.Index = int.Parse(m.Groups[1].Value);
                msg.Status = m.Groups[2].Value;
                msg.Sender = m.Groups[3].Value;
                msg.Alphabet = m.Groups[4].Value;
                msg.Sent = m.Groups[5].Value;
                msg.Message = m.Groups[6].Value;
                messages.Add(msg);

                m = m.NextMatch();
            }

            return messages;
        }

        #region Communication
        private SerialPort OpenPort(string portName)
        {
            SerialPort port = new SerialPort();
            port.PortName = portName;
            port.BaudRate = 9600;
            port.DataBits = 8;
            port.StopBits = StopBits.One;
            port.Parity = Parity.None;
            port.ReadTimeout = 300;
            port.WriteTimeout = 300;
            port.Encoding = Encoding.GetEncoding("iso-8859-1");
            port.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
            port.DtrEnable = true;
            port.RtsEnable = true;
            //port.NewLine = System.Environment.NewLine;
            port.Open();
            return port;
        }

        private void ClosePort(SerialPort port)
        {
            port.Close();
            port.DataReceived -= new SerialDataReceivedEventHandler(port_DataReceived);
        }

        void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (e.EventType == SerialData.Chars)
                receiveNow.Set();
        }

        private string ReadResponse(int timeout)
        {
            string buffer = string.Empty;
            do
            {
                if (receiveNow.WaitOne(timeout, false))
                {
                    string t = port.ReadExisting();
                    buffer += t;
                }
                else
                {
                    if (buffer.Length > 0)
                        throw new ApplicationException("Response received is incomplete.");
                    else
                        throw new ApplicationException("No data received from phone.");
                }
            }
            while (!buffer.EndsWith("\r\nOK\r\n") && !buffer.EndsWith("\r\nERROR\r\n"));
            return buffer;
        }

        private string ExecCommandread(string command, int responseTimeout, string errorMessage)
        {
            try
            {
                port.DiscardOutBuffer();
                port.DiscardInBuffer();
                receiveNow.Reset();
                port.Write(command + "\r");

                string input = ReadResponse(responseTimeout);
                if ((input.Length == 0) || (!input.EndsWith("\r\nOK\r\n")))
                    throw new ApplicationException("No success message was received.");
                return input;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(errorMessage, ex);
            }
        }

        #region Error Log
        public void ErrorLog(string Message)
        {
            StreamWriter sw = null;

            try
            {

                string sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";
                //string sPathName = @"E:\";
                string sPathName = @"C:\ErrorLog\";

                string sYear = DateTime.Now.Year.ToString();
                string sMonth = DateTime.Now.Month.ToString();
                string sDay = DateTime.Now.Day.ToString();

                string sErrorTime = sDay + "-" + sMonth + "-" + sYear;

                sw = new StreamWriter(sPathName + sErrorTime + ".txt", true);

                ViewBag.sd = "asd";

                sw.WriteLine(sLogFormat + Message);
                sw.Flush();

            }
            catch (Exception ex)
            {
                ErrorLog(ex.ToString());
            }
            finally
            {
                if (sw != null)
                {
                    sw.Dispose();
                    sw.Close();
                }
            }

        }
        #endregion 

        #endregion

        #endregion


        #region ChartData
        public ActionResult HoursWaterData()
        {
            return View();
        }


        public ContentResult GetChartWaterData()
        {
            DateTime now = DateTime.Now;
            DateTime yesterday = now.AddDays(-1);
            //filter: z => z.DateReceived >= yesterday && z.DateReceived <= now ,
            var WaterLevel = _context.WaterLevelRepo.Get(z => z.DateReceived >= yesterday, orderBy: a => a.OrderBy(n => n.DateReceived), includeProperties: o => o.Devices).Select(a => MapperInstance.Map<WaterLevel, WaterLevelViewModel>(a));
            var Devices = _context.DevicesRepo.Get(orderBy: a => a.OrderBy(n => n.NameOfPlace), includeProperties: o => o.Category).Select(a => MapperInstance.Map<Devices, DevicesViewModel>(a));

            List<Dictionary<string, object>> dataResult = new List<Dictionary<string, object>>();
            foreach (var item in Devices)
            {

                Dictionary<string, object> aSeries = new Dictionary<string, object>();
                if (item.CategoryId == 3)
                {
                    aSeries["data"] = new List<object[]>();
                    aSeries["name"] = item.NameOfPlace;
                    foreach (var itemwater in WaterLevel)
                    {
                        if (itemwater.DeviceId == item.Id)
                        {

                            object[] values = new object[2];
                            TimeSpan t = (itemwater.DateReceived - new DateTime(1970, 1, 1, 0, 0, 0));
                            long timestamp = (long)t.TotalMilliseconds;
                            values[0] = timestamp;
                            values[1] = Convert.ToDecimal(itemwater.Message);
                            ((List<object[]>)aSeries["data"]).Add(values);

                        }
                    }

                    dataResult.Add(aSeries);
                }

            }

            return Content(JsonConvert.SerializeObject(dataResult), "application/json");
        }

        public ActionResult HoursFlowMeterData()
        {
            return View();
        }

        public ContentResult GetChartFlowMeterData()
        {
            DateTime now = DateTime.Now;
            DateTime yesterday = now.AddDays(-1);
            //filter: z => z.DateReceived >= yesterday

            var WaterLevel = _context.FlowMeterRepo.Get(filter: z => z.DateReceived >= yesterday, orderBy: a => a.OrderBy(n => n.DateReceived), includeProperties: o => o.Devices).Select(a => MapperInstance.Map<FlowMeter, FlowMeterViewModel>(a));
            var Devices = _context.DevicesRepo.Get(orderBy: a => a.OrderBy(n => n.NameOfPlace), includeProperties: o => o.Category).Select(a => MapperInstance.Map<Devices, DevicesViewModel>(a));

            List<Dictionary<string, object>> dataResult = new List<Dictionary<string, object>>();
            foreach (var item in Devices)
            {

                Dictionary<string, object> aSeries = new Dictionary<string, object>();
                if (item.CategoryId == 2)
                {
                    aSeries["data"] = new List<object[]>();
                    aSeries["name"] = item.NameOfPlace;


                    foreach (var itemwater in WaterLevel)
                    {
                        if (itemwater.DeviceId == item.Id)
                        {

                            object[] values = new object[2];
                            DateTime utcDateTime = new DateTime(itemwater.DateReceived.Year, itemwater.DateReceived.Month, itemwater.DateReceived.Day, itemwater.DateReceived.Hour, itemwater.DateReceived.Minute, itemwater.DateReceived.Second, DateTimeKind.Utc);
                            TimeSpan t = (itemwater.DateReceived - new DateTime(1970, 1, 1, 0, 0, 0));
                            long timestamp = (long)t.TotalMilliseconds;
                            values[0] = timestamp;
                            values[1] = Convert.ToDecimal(itemwater.Message);
                            ((List<object[]>)aSeries["data"]).Add(values);

                        }
                    }

                    dataResult.Add(aSeries);
                }
            }

            return Content(JsonConvert.SerializeObject(dataResult), "application/json");
        }

        public ActionResult HoursRainGaugedData()
        {
            return View();
        }

        public ContentResult GetChartRainGaugedData()
        {
            DateTime now = DateTime.Now;
            DateTime yesterday = now.AddDays(-1);
            var WaterLevel = _context.RainGaugeRepo.Get(filter: z => z.DateReceived >= yesterday, orderBy: a => a.OrderBy(n => n.DateReceived), includeProperties: o => o.Devices).Select(a => MapperInstance.Map<RainGauge, RainGaugeViewModel>(a));
            var Devices = _context.DevicesRepo.Get(orderBy: a => a.OrderBy(n => n.NameOfPlace), includeProperties: o => o.Category).Select(a => MapperInstance.Map<Devices, DevicesViewModel>(a));

            List<Dictionary<string, object>> dataResult = new List<Dictionary<string, object>>();
            foreach (var item in Devices)
            {

                Dictionary<string, object> aSeries = new Dictionary<string, object>();
                if (item.CategoryId == 1)
                {
                    aSeries["data"] = new List<object[]>();
                    aSeries["name"] = item.NameOfPlace;


                    foreach (var itemwater in WaterLevel)
                    {
                        if (itemwater.DeviceId == item.Id)
                        {

                            object[] values = new object[2];
                            DateTime utcDateTime = new DateTime(itemwater.DateReceived.Year, itemwater.DateReceived.Month, itemwater.DateReceived.Day, itemwater.DateReceived.Hour, itemwater.DateReceived.Minute, itemwater.DateReceived.Second, DateTimeKind.Utc);
                            TimeSpan t = (itemwater.DateReceived - new DateTime(1970, 1, 1, 0, 0, 0));
                            long timestamp = (long)t.TotalMilliseconds;
                            values[0] = timestamp;
                            values[1] = Convert.ToDecimal(itemwater.Message);
                            ((List<object[]>)aSeries["data"]).Add(values);

                        }
                    }

                    dataResult.Add(aSeries);
                }
            }

            return Content(JsonConvert.SerializeObject(dataResult), "application/json");
        }

        //History

        public ActionResult WaterDataHistory()
        {
            return View();
        }





        public ActionResult PrintWaterDataHistory(string id)
        {
            var startdate = id.Split('-')[0];
            var enddate = id.Split('-')[1];
            DateTime s = Convert.ToDateTime(startdate);
            DateTime e = Convert.ToDateTime(enddate);
            var WaterLevel = _context.WaterLevelRepo.Get(c => c.DateReceived >= s && c.DateReceived <= e, orderBy: a => a.OrderByDescending(n => n.DateReceived), includeProperties: o => o.Devices).Select(a => MapperInstance.Map<WaterLevel, WaterLevelViewModel>(a));
            return PartialView(WaterLevel);
        }
        public ContentResult GetChartWaterDataHistory()
        {

            var WaterLevel = _context.WaterLevelRepo.Get(orderBy: a => a.OrderBy(n => n.DateReceived), includeProperties: o => o.Devices).Select(a => MapperInstance.Map<WaterLevel, WaterLevelViewModel>(a));
            var Devices = _context.DevicesRepo.Get(orderBy: a => a.OrderBy(n => n.NameOfPlace), includeProperties: o => o.Category).Select(a => MapperInstance.Map<Devices, DevicesViewModel>(a));

            List<Dictionary<string, object>> dataResult = new List<Dictionary<string, object>>();
            foreach (var item in Devices)
            {

                Dictionary<string, object> aSeries = new Dictionary<string, object>();
                if (item.CategoryId == 3)
                {
                    aSeries["name"] = item.NameOfPlace;
                    aSeries["data"] = new List<object[]>();

                    foreach (var itemwater in WaterLevel)
                    {
                        if (itemwater.DeviceId == item.Id)
                        {

                            object[] values = new object[2];
                            DateTime utcDateTime = new DateTime(itemwater.DateReceived.Year, itemwater.DateReceived.Month, itemwater.DateReceived.Day, itemwater.DateReceived.Hour, itemwater.DateReceived.Minute, itemwater.DateReceived.Second, DateTimeKind.Utc);
                            TimeSpan t = (itemwater.DateReceived - new DateTime(1970, 1, 1, 0, 0, 0));
                            long timestamp = (long)t.TotalMilliseconds;
                            values[0] = timestamp;
                            values[1] = Convert.ToDecimal(itemwater.Message);
                            ((List<object[]>)aSeries["data"]).Add((values));

                        }
                    }

                    dataResult.Add(aSeries);
                }

            }

            return Content(JsonConvert.SerializeObject(dataResult), "application/json");
        }

        public ActionResult FlowMeterDataHistory()
        {
            return View();
        }
        public ActionResult PrintFlowMeterDataHistory(string id)
        {
            var startdate = id.Split('-')[0];
            var enddate = id.Split('-')[1];
            DateTime s = Convert.ToDateTime(startdate);
            DateTime e = Convert.ToDateTime(enddate);
            var FlowMeterDataHistory = _context.FlowMeterRepo.Get(c => c.DateReceived >= s && c.DateReceived <= e, orderBy: a => a.OrderByDescending(n => n.DateReceived), includeProperties: o => o.Devices).Select(a => MapperInstance.Map<FlowMeter, FlowMeterViewModel>(a));
            return PartialView(FlowMeterDataHistory);
        }
        public ContentResult GetChartFlowMeterDataHistory()
        {
            var FlowMeter = _context.FlowMeterRepo.Get(orderBy: a => a.OrderBy(n => n.DateReceived), includeProperties: o => o.Devices).Select(a => MapperInstance.Map<FlowMeter, FlowMeterViewModel>(a));
            var Devices = _context.DevicesRepo.Get(orderBy: a => a.OrderBy(n => n.NameOfPlace), includeProperties: o => o.Category).Select(a => MapperInstance.Map<Devices, DevicesViewModel>(a));

            List<Dictionary<string, object>> dataResult = new List<Dictionary<string, object>>();
            foreach (var item in Devices)
            {

                Dictionary<string, object> aSeries = new Dictionary<string, object>();
                if (item.CategoryId == 2)
                {
                    aSeries["name"] = item.NameOfPlace;
                    aSeries["data"] = new List<object[]>();

                    foreach (var itemwater in FlowMeter)
                    {
                        if (itemwater.DeviceId == item.Id)
                        {

                            object[] values = new object[2];
                            DateTime utcDateTime = new DateTime(itemwater.DateReceived.Year, itemwater.DateReceived.Month, itemwater.DateReceived.Day, itemwater.DateReceived.Hour, itemwater.DateReceived.Minute, itemwater.DateReceived.Second, DateTimeKind.Utc);
                            TimeSpan t = (itemwater.DateReceived - new DateTime(1970, 1, 1, 0, 0, 0));
                            long timestamp = (long)t.TotalMilliseconds;
                            values[0] = timestamp;
                            values[1] = Convert.ToDecimal(itemwater.Message);
                            ((List<object[]>)aSeries["data"]).Add((values));

                        }
                    }

                    dataResult.Add(aSeries);
                }

            }

            return Content(JsonConvert.SerializeObject(dataResult), "application/json");
        }

        public ActionResult RainGaugedDataHistory()
        {
            return View();
        }
        public ActionResult PrintRainGaugedDataHistory(string id)
        {
            var startdate = id.Split('-')[0];
            var enddate = id.Split('-')[1];
            DateTime s = Convert.ToDateTime(startdate);
            DateTime e = Convert.ToDateTime(enddate);
            var RainGaugedDataHistory = _context.RainGaugeRepo.Get(c => c.DateReceived >= s && c.DateReceived <= e, orderBy: a => a.OrderByDescending(n => n.DateReceived), includeProperties: o => o.Devices).Select(a => MapperInstance.Map<RainGauge, RainGaugeViewModel>(a));
            return PartialView(RainGaugedDataHistory);
        }
        public ContentResult GetChartRainGaugedDataHistory()
        {
            var RainGauged = _context.RainGaugeRepo.Get(orderBy: a => a.OrderBy(n => n.DateReceived), includeProperties: o => o.Devices).Select(a => MapperInstance.Map<RainGauge, RainGaugeViewModel>(a));
            var Devices = _context.DevicesRepo.Get(orderBy: a => a.OrderBy(n => n.NameOfPlace), includeProperties: o => o.Category).Select(a => MapperInstance.Map<Devices, DevicesViewModel>(a));

            List<Dictionary<string, object>> dataResult = new List<Dictionary<string, object>>();
            foreach (var item in Devices)
            {

                Dictionary<string, object> aSeries = new Dictionary<string, object>();
                if (item.CategoryId == 1)
                {
                    aSeries["name"] = item.NameOfPlace;
                    aSeries["data"] = new List<object[]>();

                    foreach (var itemwater in RainGauged)
                    {
                        if (itemwater.DeviceId == item.Id)
                        {

                            object[] values = new object[2];
                            DateTime utcDateTime = new DateTime(itemwater.DateReceived.Year, itemwater.DateReceived.Month, itemwater.DateReceived.Day, itemwater.DateReceived.Hour, itemwater.DateReceived.Minute, itemwater.DateReceived.Second, DateTimeKind.Utc);
                            TimeSpan t = (itemwater.DateReceived - new DateTime(1970, 1, 1, 0, 0, 0));
                            long timestamp = (long)t.TotalMilliseconds;
                            values[0] = timestamp;
                            values[1] = Convert.ToDecimal(itemwater.Message);
                            ((List<object[]>)aSeries["data"]).Add((values));

                        }
                    }

                    dataResult.Add(aSeries);
                }

            }

            return Content(JsonConvert.SerializeObject(dataResult), "application/json");
        }

        #endregion


        #region Notification


        public ActionResult _GetUserNotify()
        {
            var notifyUser = _context.UserNotificationRepo.Get(orderBy: a => a.OrderByDescending(n => n.Id)).Take(30).Select(a => MapperInstance.Map<UserNotification, UserNotificationViewModel>(a));

            return PartialView("_GetUserNotify", notifyUser);
        }
        public ActionResult _GetNotifyMessage()
        {
            var notify = _context.MessageNotificationRepo.Get(orderBy: a => a.OrderByDescending(n => n.Id)).Take(30).Select(a => MapperInstance.Map<MessageNotification, MessageNotificationViewModel>(a));


            return PartialView("_GetNotifyMessage", notify);
        }

        public ActionResult UserLog()
        {
            return View();

        }
        public ActionResult GetAllUserLog()
        {
            var notify = _context.UserNotificationRepo.Get(orderBy: a => a.OrderByDescending(n => n.Id)).Select(a => MapperInstance.Map<UserNotification, UserNotificationViewModel>(a));
            return PartialView("_GetAllUserLog", notify);
        }
        public ActionResult MessagesLog()
        {
            return View();
        }

        public ActionResult GetAllMessagesNotification()
        {
            var notify = _context.MessageNotificationRepo.Get(orderBy: a => a.OrderByDescending(n => n.Id)).Select(a => MapperInstance.Map<MessageNotification, MessageNotificationViewModel>(a));
            return PartialView("_GetAllMessagesNotification", notify);
        }


        [HttpPost]
        public ActionResult EditNotify()
        {
            var notify = _context.MessageNotificationRepo.Get(orderBy: a => a.OrderByDescending(n => n.Id)).Select(a => MapperInstance.Map<MessageNotification, MessageNotificationViewModel>(a));

            return SaveBasicForm(() =>
            {
                foreach (var item in notify)
                {
                    var dev = _context.MessageNotificationRepo.GetById(item.Id);
                    dev.MessageStatus = Core.Enums.MessageNotify.Read;
                    _context.MessageNotificationRepo.Update(dev);
                    MessageHub.NotifyDataMessageToAllClients();
                    _context.Complete();

                }

            });
        }
        [HttpPost]
        public ActionResult EditNotifyUser()
        {
            var notifys = _context.UserNotificationRepo.Get(orderBy: a => a.OrderByDescending(n => n.Id)).Select(a => MapperInstance.Map<UserNotification, UserNotificationViewModel>(a));

            return SaveBasicForm(() =>
            {
                foreach (var item in notifys)
                {
                    var dev = _context.UserNotificationRepo.GetById(item.Id);
                    dev.NotifyUserStatus = Core.Enums.UserNotify.Read;

                    _context.UserNotificationRepo.Update(dev);
                    MessageHub.NotifyDataMessageToAllClients();
                    _context.Complete();

                }

            });
        }
        #endregion
    }
}