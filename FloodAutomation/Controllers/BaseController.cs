﻿using AutoMapper;
using FloodAutomation.App_Start;
using FloodAutomation.Models;
using FloodAutomation.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using FloodAutomation.Utilities;
namespace FloodAutomation.Controllers
{
    public class BaseController : Controller
    {
        private IMapper mapper;
        protected IMapper MapperInstance
        {
            get
            {
                if (mapper == null)
                {
                    mapper = AutoMapperConfig.MapperConfiguration.CreateMapper();
                }
                return mapper;
            }

        }

        protected ActionResult ReturnErrorResponse(RequestResponseModel responseModel)
        {
            responseModel.Title = "Error!";
            responseModel.InfoType = RequestResultInfoType.ErrorOrDanger;
            return Json(new
            {
                Success = false,
                NotifyType = NotifyType.PageInline,
                Html = this.RenderPartialView(@"_RequestResultPageInlineMessage", responseModel)

            }, JsonRequestBehavior.AllowGet);
        }

        protected ActionResult ReturnSuccessResponse(RequestResponseModel responseModel, object data)
        {
            responseModel.Title = "Success!";
            responseModel.InfoType = RequestResultInfoType.Success;
            return Json(new
            {
                data = data,
                Success = true,
                NotifyType = NotifyType.PageInline,
                Html = this.RenderPartialView(@"_RequestResultPageInlineMessage", responseModel)

            }, JsonRequestBehavior.AllowGet);
        }

        protected string GetMessage(ResponseMessageType type)
        {
            string message = "";
            switch (type)
            {
                case ResponseMessageType.Error:
                    message = "An error occured while processing your request!";
                    break;
                case ResponseMessageType.Save:
                    message = "Record has successfully saved.";
                    break;
                case ResponseMessageType.Update:
                    message = "Updates on record has successfully saved.";
                    break;
                case ResponseMessageType.Delete:
                    message = "Record has successfully deleted.";
                    break;
                case ResponseMessageType.AccessDenied:
                    message = "You are not authorized to access the resource.";
                    break;
                default:
                    message = "An unknown error has occured. Please contact your system administrator";
                    break;
            }

            return message;
        }

        protected ActionResult SaveBasicForm(Action saveModel)
        {
            RequestResponseModel response = new RequestResponseModel();
            try
            {
                if (ModelState.IsValid)
                {
                    saveModel();
                    response.Message = GetMessage(ResponseMessageType.Save);
                    return ReturnSuccessResponse(response, null);
                }
                response.Message = GetValidationErrors();
            }
            catch (Exception e)
            {
                response.Message = GetMessage(ResponseMessageType.Error);
            }
            return ReturnErrorResponse(response);
        }

        protected ActionResult SaveBasicForm(Func<object> saveModel)
        {
            RequestResponseModel response = new RequestResponseModel();
            try
            {
                if (ModelState.IsValid)
                {
                    var data = saveModel();
                    response.Message = GetMessage(ResponseMessageType.Save);
                    return ReturnSuccessResponse(response, data);
                }

                response.Message = GetValidationErrors();
            }
            catch (Exception e)
            {
                response.Message = GetMessage(ResponseMessageType.Error);
            }
            return ReturnErrorResponse(response);
        }

        protected string GetValidationErrors()
        {
            var allErrors = ModelState.Values.SelectMany(v => v.Errors);
            StringBuilder validationErrors = new StringBuilder();
            foreach (var item in allErrors)
            {
                validationErrors.Append(String.Format("{0} {1}<br>", item.ErrorMessage, Environment.NewLine));
            }

            return validationErrors.ToString();
        }


    }
}