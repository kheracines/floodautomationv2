﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FloodAutomation.Core.Domain;
using FloodAutomation.Core.DTO;
namespace FloodAutomation.Infrastructure
{
    public static class ModelFactory
    {
        public static GSMData ToDomain(this GSMDataDTO model)
        {
            return new GSMData
            {
                Id = model.Id,
                CNumber = model.CNumber,
                Message = model.Message,
                DateReceived = model.DateReceived,
            };
        }
        public static GSMDataDTO ToDTO(this GSMData model)
        {
            return new GSMDataDTO
            {
                Id = model.Id,
                CNumber = model.CNumber,
                Message = model.Message,
                DateReceived = model.DateReceived,
            };
        }

        public static User ToDomain(this UserDTO model)
        {
            return new User
            {
                Id = model.Id,
                NameIdentifier = model.NameIdentifier,
                UserName = model.UserName,
                PhoneNumber = model.PhoneNumber,
                Email = model.Email,
                Gender = model.Gender,
                Status = model.Status,
                DateRegistered = model.DateRegistered,
                DateModified = model.DateModified,

            };
        }
        public static UserDTO ToDTO(this User model)
        {
            return new UserDTO
            {
                Id = model.Id,
                NameIdentifier = model.NameIdentifier,
                UserName = model.UserName,
                PhoneNumber = model.PhoneNumber,
                Email = model.Email,
                Gender = model.Gender,
                Status = model.Status,
                DateRegistered = model.DateRegistered,
                DateModified = model.DateModified,
            };
        }
        public static Role ToDomain(this RoleDTO model)
        {
            return new Role
            {
                Id = model.Id,
                Name = model.Name,
            };
        }
        public static RoleDTO ToDTO(this Role model)
        {
            return new RoleDTO
            {
                Id = model.Id,
                Name = model.Name,
            };
        }
        public static Category ToDomain(this CategoryDTO model)
        {
            return new Category
            {
                Id = model.Id,
                CategoryName = model.CategoryName,
            };
        }
        public static CategoryDTO ToDTO(this Category model)
        {
            return new CategoryDTO
            {
                Id = model.Id,
                CategoryName = model.CategoryName,
            };
        }
        public static Devices ToDomain(this DevicesDTO model)
        {
            var data = new Devices
            {
                Id = model.Id,
                NameOfPlace = model.NameOfPlace,
                Longitude = model.Longitude,
                Latitude = model.Latitude,
                PhoneNumber = model.PhoneNumber,
                Low = model.Low,
                Moderate = model.Moderate,
                High = model.High,
                Very_High = model.Very_High,
            };
            if (model.Category != null)
            {
                data.Category = model.Category.ToDomain();
            }
            return data;
        }
        public static DevicesDTO ToDTO(this Devices model)
        {
            var data = new DevicesDTO
            {
                Id = model.Id,
                NameOfPlace = model.NameOfPlace,
                Longitude = model.Longitude,
                Latitude = model.Latitude,
                PhoneNumber = model.PhoneNumber,
                Low = model.Low,
                Moderate = model.Moderate,
                High = model.High,
                Very_High = model.Very_High,
            };
            if (model.Category != null)
            {
                data.Category = model.Category.ToDTO();
            }
            return data;
        }

        public static FlowMeter ToDomain(this FlowMeterDTO model)
        {
            var data = new FlowMeter
            {
                Id = model.Id,
                SenderNumber = model.SenderNumber,
                Message = model.Message,
                DateReceived = model.DateReceived,
                Status = model.Status,
                Zindex = model.Zindex,
                ConNotifyId = model.ConNotifyId,

            };
            if (model.Devices != null)
            {
                data.Devices = model.Devices.ToDomain();
            }
            return data;
        }
        public static FlowMeterDTO ToDTO(this FlowMeter model)
        {
            var data = new FlowMeterDTO
            {
                Id = model.Id,
                SenderNumber = model.SenderNumber,
                Message = model.Message,
                DateReceived = model.DateReceived,
                Status = model.Status,
                Zindex = model.Zindex,
                ConNotifyId = model.ConNotifyId,

            };
            if (model.Devices != null)
            {
                data.Devices = model.Devices.ToDTO();
            }
            return data;
        }

        public static WaterLevel ToDomain(this WaterLevelDTO model)
        {
            var data = new WaterLevel
            {
                Id = model.Id,
                SenderNumber = model.SenderNumber,
                Message = model.Message,
                DateReceived = model.DateReceived,
                Status = model.Status,
                Zindex = model.Zindex,
                ConNotifyId = model.ConNotifyId,
            };
            if (model.Devices != null)
            {
                data.Devices = model.Devices.ToDomain();
            }
            return data;
        }
        public static WaterLevelDTO ToDTO(this WaterLevel model)
        {
            var data = new WaterLevelDTO
            {
                Id = model.Id,
                SenderNumber = model.SenderNumber,
                Message = model.Message,
                DateReceived = model.DateReceived,
                Status = model.Status,
                Zindex = model.Zindex,
                ConNotifyId = model.ConNotifyId,

            };
            if (model.Devices != null)
            {
                data.Devices = model.Devices.ToDTO();
            }
            return data;
        }


        public static RainGauge ToDomain(this RainGaugeDTO model)
        {
            var data = new RainGauge
            {
                Id = model.Id,
                SenderNumber = model.SenderNumber,
                Message = model.Message,
                DateReceived = model.DateReceived,
                Status = model.Status,
                Zindex = model.Zindex,
                ConNotifyId = model.ConNotifyId,

            };
            if (model.Devices != null)
            {
                data.Devices = model.Devices.ToDomain();
            }
            return data;
        }
        public static RainGaugeDTO ToDTO(this RainGauge model)
        {
            var data = new RainGaugeDTO
            {
                Id = model.Id,
                SenderNumber = model.SenderNumber,
                Message = model.Message,
                DateReceived = model.DateReceived,
                Status = model.Status,
                Zindex = model.Zindex,
                ConNotifyId = model.ConNotifyId,
               

            };
            if (model.Devices != null)
            {
                data.Devices = model.Devices.ToDTO();
            }
            return data;
        }

        public static MessageNotification ToDomain(this MessageNotificationDTO model)
        {
            return new MessageNotification
            {
                Id = model.Id,
                MessageStatus = model.MessageStatus,
                ConNotifyId = model.ConNotifyId,
                DeviceName = model.DeviceName,
                CategoryName = model.CategoryName,
                SenderNumber = model.SenderNumber,
                Message = model.Message,
                DateReceived = model.DateReceived,
                StatusLevel = model.StatusLevel,

            };
         

        }
        public static MessageNotificationDTO ToDTO(this MessageNotification model)
        {
            return new MessageNotificationDTO

            {
                Id = model.Id,
                MessageStatus = model.MessageStatus,
                ConNotifyId = model.ConNotifyId,
                DeviceName = model.DeviceName,
                CategoryName = model.CategoryName,
                SenderNumber = model.SenderNumber,
                Message = model.Message,
                DateReceived = model.DateReceived,
                StatusLevel = model.StatusLevel,

            };
         
        }

        public static UserNotification ToDomain(this UserNotificationDTO model)
        {
            return new UserNotification
            {
                Id = model.Id,
               userEmail = model.userEmail,
                DateTime = model.DateTime,
               SessionStatus = model.SessionStatus,
               NotifyUserStatus =model.NotifyUserStatus,

            };


        }
        public static UserNotificationDTO ToDTO(this UserNotification model)
        {
            return new UserNotificationDTO

            {
                Id = model.Id,
                userEmail = model.userEmail,
                DateTime = model.DateTime,
                SessionStatus = model.SessionStatus,
                NotifyUserStatus = model.NotifyUserStatus,

            };

        }
    }
}
