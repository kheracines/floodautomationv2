﻿using FloodAutomation.Core.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloodAutomation.Infrastructure
{
    public class MyDbContext : DbContext
    {
        public MyDbContext() : base("DefaultConnection")
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
        public DbSet<GSMData> GSMData { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<Devices> Devices { get; set; }
        public DbSet<FlowMeter> FlowMeter { get; set; }
        public DbSet<WaterLevel> WaterLevel { get; set; }
        public DbSet<RainGauge> RainGauge { get; set; }
        public DbSet<MessageNotification> MessageNotification { get; set; }
        public DbSet<UserNotification> UserNotification { get; set; }

    }
}
