﻿using FloodAutomation.Core.Interfaces.Repository;

namespace FloodAutomation.Infrastructure.Repositories
{
    public class UnitofWork : IUnitofWork
    {
        private MyDbContext _context = new MyDbContext();
        public UnitofWork()
        {

        }
        private IGSMDataRepo _GSMDataRepo;
        private IUserRepo _UserRepo;
        private IRoleRepo _RoleRepo;
        private ICategoryRepo _CategoryRepo;
        private IDevicesRepo _DevicesRepo;
        public IWaterLevelRepo _WaterLevelRepo;
        public IRainGaugeRepo _RainGaugeRepo;
        public IFlowMeterRepo _FlowMeterRepo;
        public IMessageNotificationRepo _MessageNotificationRepo;
        public IUserNotificationRepo _UserNotificationRepo;

        public IGSMDataRepo GSMDataRepo
        {
            get
            {
                return _GSMDataRepo ?? new GSMDataRepo(_context);
            }
        }
        public IUserRepo UserRepo
        {
            get
            {
                return _UserRepo ?? new UserRepo(_context);
            }
        }
        public IRoleRepo RoleRepo
        {
            get
            {
                return _RoleRepo ?? new RoleRepo(_context);
            }
        }
        public ICategoryRepo CategoryRepo
        {
            get
            {
                return _CategoryRepo ?? new CategoryRepo(_context);
            }
        }
        public IDevicesRepo DevicesRepo
        {
            get
            {
                return _DevicesRepo ?? new DevicesRepo(_context);
            }
        }
        public IFlowMeterRepo FlowMeterRepo
        {
            get
            {
                return _FlowMeterRepo ?? new FlowMeterRepo(_context);
            }
        }
        public IRainGaugeRepo RainGaugeRepo
        {
            get
            {
                return _RainGaugeRepo ?? new RainGaugeRepo(_context);
            }
        }
        public IWaterLevelRepo WaterLevelRepo
        {
            get
            {
                return _WaterLevelRepo ?? new WaterLevelRepo(_context);
            }
        }
        public IMessageNotificationRepo MessageNotificationRepo
        {
            get
            {
                return _MessageNotificationRepo ?? new MessageNotificationRepo(_context);
            }
        }
        public IUserNotificationRepo UserNotificationRepo
        {
            get
            {
                return _UserNotificationRepo ?? new UserNotificationRepo(_context);
            }
        }



        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
