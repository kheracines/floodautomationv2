﻿using FloodAutomation.Core.Domain;
using FloodAutomation.Core.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloodAutomation.Infrastructure.Repositories
{
 
    public class UserNotificationRepo : Repository<UserNotification>, IUserNotificationRepo
    {
        public UserNotificationRepo(MyDbContext context) : base(context)
        {

        }
    }
}
