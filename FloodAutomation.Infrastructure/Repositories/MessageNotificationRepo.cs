﻿using FloodAutomation.Core.Domain;
using FloodAutomation.Core.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloodAutomation.Infrastructure.Repositories
{
    public class MessageNotificationRepo : Repository<MessageNotification>, IMessageNotificationRepo
    {
        public MessageNotificationRepo(MyDbContext context) : base(context)
        {

        }
    }
}
