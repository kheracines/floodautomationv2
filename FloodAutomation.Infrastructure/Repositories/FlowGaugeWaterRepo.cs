﻿using FloodAutomation.Core.Domain;
using FloodAutomation.Core.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloodAutomation.Infrastructure.Repositories
{
    class FlowGaugeWaterRepo
    {
    }

    public class FlowMeterRepo : Repository<FlowMeter>, IFlowMeterRepo
    {
        public FlowMeterRepo(MyDbContext context) : base(context)
        {

        }
    }
    public class RainGaugeRepo : Repository<RainGauge>, IRainGaugeRepo
    {
        public RainGaugeRepo(MyDbContext context) : base(context)
        {

        }
    }
    public class WaterLevelRepo : Repository<WaterLevel>, IWaterLevelRepo
    {
        public WaterLevelRepo(MyDbContext context) : base(context)
        {

        }
    }

}
