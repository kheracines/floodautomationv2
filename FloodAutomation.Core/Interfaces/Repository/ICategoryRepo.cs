﻿using FloodAutomation.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloodAutomation.Core.Interfaces.Repository
{
    public interface ICategoryRepo : IRepository<Category>
    {
    }
}
