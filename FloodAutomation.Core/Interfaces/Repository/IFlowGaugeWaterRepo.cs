﻿using FloodAutomation.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloodAutomation.Core.Interfaces.Repository
{
    interface IFlowGaugeWaterRepo
    {
    }
    public interface IFlowMeterRepo : IRepository<FlowMeter>
    {
    }
    public interface IWaterLevelRepo : IRepository<WaterLevel>
    {
    }
    public interface IRainGaugeRepo : IRepository<RainGauge>
    {
    }
}
