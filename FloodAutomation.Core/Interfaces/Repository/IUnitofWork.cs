﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloodAutomation.Core.Interfaces.Repository
{
    public interface IUnitofWork : IDisposable
    {
        IGSMDataRepo GSMDataRepo { get; }
        IUserRepo UserRepo { get; }
        IRoleRepo RoleRepo { get; }
        ICategoryRepo CategoryRepo { get; }
        IDevicesRepo DevicesRepo { get; }
        IRainGaugeRepo RainGaugeRepo { get; }
        IWaterLevelRepo WaterLevelRepo { get; }
        IFlowMeterRepo FlowMeterRepo { get; }
        IMessageNotificationRepo MessageNotificationRepo { get; }
        IUserNotificationRepo UserNotificationRepo { get; }
        int Complete();
    }
}
