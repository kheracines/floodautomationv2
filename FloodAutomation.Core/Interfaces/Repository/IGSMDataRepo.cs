﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FloodAutomation.Core.Domain;
namespace FloodAutomation.Core.Interfaces.Repository
{
    public interface IGSMDataRepo : IRepository<GSMData>
    {
    }
}
