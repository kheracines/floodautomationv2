﻿using FloodAutomation.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloodAutomation.Core.DTO
{
    public class UserNotificationDTO
    {
        public int Id { get; set; }
        public string userEmail { get; set; }
        public DateTime DateTime { get; set; }
        public SessionStatus SessionStatus { get; set; }
        public UserNotify NotifyUserStatus { get; set; }
    }
}
