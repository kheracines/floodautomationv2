﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloodAutomation.Core.DTO
{
    public class GSMDataDTO
    {
        public Guid Id { get; set; }
        public string CNumber { get; set; }
        public string Message { get; set; }
        public DateTime DateReceived { get; set; }  
    }
}
