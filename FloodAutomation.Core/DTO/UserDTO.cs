﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FloodAutomation.Core.Enums;
namespace FloodAutomation.Core.DTO
{
   public class UserDTO
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public DateTime DateRegistered { get; set; }
        public DateTime DateModified { get; set; }
        public string NameIdentifier { get; set; }
        public Gender Gender { get; set; }
        public UserStatuses Status { get; set; }
    }
}
