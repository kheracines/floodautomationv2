﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloodAutomation.Core.DTO
{
    class FlowGaugeWaterDTO
    {
    }

    public class FlowMeterDTO
    {
        public int Id { get; set; }
        public Guid? DeviceId { get; set; }
        public string SenderNumber { get; set; }
        public string Message { get; set; }
        public DateTime DateReceived { get; set; }
        public string Status { get; set; }
        public string Zindex { get; set; }
        public string ConNotifyId { get; set; }

        public DevicesDTO Devices { get; set; }
    }

  
    public class RainGaugeDTO
    {
        public int Id { get; set; }
        public Guid? DeviceId { get; set; }
        public string SenderNumber { get; set; }
        public string Message { get; set; }
        public DateTime DateReceived { get; set; }
        public string Status { get; set; }
        public string Zindex { get; set; }
        public string ConNotifyId { get; set; }

        public DevicesDTO Devices { get; set; }
    }

    public class WaterLevelDTO
    {
        public int Id { get; set; }
        public Guid? DeviceId { get; set; }
        public string SenderNumber { get; set; }
        public string Message { get; set; }
        public DateTime DateReceived { get; set; }
        public string Status { get; set; }
        public string Zindex { get; set; }
        public string ConNotifyId { get; set; }

        public DevicesDTO Devices { get; set; }
    }

}
