﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloodAutomation.Core.DTO
{
    public class DevicesDTO
    {
        public Guid Id { get; set; }
        public string NameOfPlace { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string PhoneNumber { get; set; }
        public int? CategoryId { get; set; }
        public decimal Low { get; set; }
        public decimal Moderate { get; set; }
        public decimal High { get; set; }
        public decimal Very_High { get; set; }

        public CategoryDTO Category { get; set; }
    }
}
