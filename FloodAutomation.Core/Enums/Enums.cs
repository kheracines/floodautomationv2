﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloodAutomation.Core.Enums
{
   public enum MessageNotify
    {
        UnRead = 0,
        Read = 1
    }
    public enum UserNotify
    {
        UnRead = 0,
        Read = 1
    }
    public enum Gender
    {
        //undefined = 0,
        Male = 0,
        Female = 1
    }
    public enum SessionStatus
    {
        LogIn = 0,
        LogOut = 1
    }
    public enum RequestResultInfoType
    {
        Information = 0,
        Success = 1,
        Warning = 2,
        ErrorOrDanger = 3
    }
    public enum UserStatuses
    {
        Active = 1,
        InActive = 2,
        Block = 3,
        WaitingForVerification = 4,
    }

    public enum NotifyType
    {
        PageInline = 0,
        DialogInline = 1,
        Dialog = 2,
        PageFixedPopUp = 3
    }

    public enum ResponseMessageType
    {
        Undefined = 0,
        [Description("Error")]
        Error = 1,
        [Description("Save")]
        Save = 2,
        [Description("Update")]
        Update = 3,
        [Description("Delete")]
        Delete = 4,
        [Description("Access Denied")]
        AccessDenied = 5
    }
}
