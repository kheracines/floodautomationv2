﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloodAutomation.Core.Domain
{
     class FlowGaugeWater
    {
    }
    [Table("FlowMeter")]
    public class FlowMeter
    {
        public int Id { get; set; }
        public Guid? DeviceId { get; set; }
        public string SenderNumber { get; set; }
        public string Message { get; set; }
        public DateTime DateReceived { get; set; }
        public string Status { get; set; }
        public string Zindex { get; set; }
        public string ConNotifyId { get; set; }

        [ForeignKey("DeviceId")]
        public Devices Devices { get; set; }
    }

    [Table("RainGauge")]
    public class RainGauge
    {
        public int Id { get; set; }
        public Guid? DeviceId { get; set; }
        public string SenderNumber { get; set; }
        public string Message { get; set; }
        public DateTime DateReceived { get; set; }
        public string Status { get; set; }
        public string Zindex { get; set; }
        public string ConNotifyId { get; set; }

        [ForeignKey("DeviceId")]
        public Devices Devices { get; set; }
    }
    [Table("WaterLevel")]
    public class WaterLevel
    {
        public int Id { get; set; }
        public Guid? DeviceId { get; set; }
        public string SenderNumber { get; set; }
        public string Message { get; set; }
        public DateTime DateReceived { get; set; }
        public string Status { get; set; }
        public string Zindex { get; set; }
        public string ConNotifyId { get; set; }

        [ForeignKey("DeviceId")]
        public Devices Devices { get; set; }
    }

}
