﻿using FloodAutomation.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloodAutomation.Core.Domain
{
    [Table("AspNetUsers")]
    public class User
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime DateRegistered { get; set; }
        public DateTime DateModified { get; set; }

        [DataType(DataType.Password)]
        [Column(name: "PasswordHash")]
        public string Password { get; set; }
        public string NameIdentifier { get; set; }
        public Gender Gender { get; set; }
        public UserStatuses Status { get; set; }
    }

}
