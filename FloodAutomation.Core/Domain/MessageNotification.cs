﻿using FloodAutomation.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloodAutomation.Core.Domain
{
    [Table("MessageNotification")]
    public class MessageNotification
    {

        public int Id { get; set; }
        public MessageNotify MessageStatus { get; set; }
        public string ConNotifyId { get; set; }
        public string DeviceName { get; set; }
        public string CategoryName { get; set; }
        public string SenderNumber { get; set; }
        public string Message { get; set; }
        public string StatusLevel { get; set; }

        public DateTime DateReceived { get; set; }
    }
}
