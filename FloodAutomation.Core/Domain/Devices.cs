﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloodAutomation.Core.Domain
{
    [Table("Devices")]
    public class Devices
    {
        public Guid Id { get; set; }
        public string NameOfPlace { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string PhoneNumber { get; set; }
        public int? CategoryId { get; set; }
        public decimal Low { get; set; }
        public decimal Moderate { get; set; }
        public decimal High { get; set; }
        public decimal Very_High { get; set; }

        [ForeignKey("CategoryId")]
        public Category Category { get; set; }
    }
}
