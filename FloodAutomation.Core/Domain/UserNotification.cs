﻿using FloodAutomation.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloodAutomation.Core.Domain
{
    [Table("UserNotification")]
    public class UserNotification
    {
        public int Id { get; set; }
        public string userEmail { get; set; }
        public DateTime DateTime { get; set; }
        public SessionStatus SessionStatus { get; set; }
        public UserNotify NotifyUserStatus { get; set; }
    }
}
