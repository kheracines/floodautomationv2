﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloodAutomation.Core.Domain
{
    [Table("AspNetRoles")]
    public class Role
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
