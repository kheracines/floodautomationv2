﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloodAutomation.Core.Domain
{
    [Table("GSMData")]
    public class GSMData
    {
        public Guid Id { get; set; }
        public string CNumber { get; set; }
        public string Message { get; set; }
        public DateTime DateReceived { get; set; }
    }
}
